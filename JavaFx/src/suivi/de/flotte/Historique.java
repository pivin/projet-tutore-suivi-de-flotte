/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package suivi.de.flotte;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javax.imageio.ImageIO;

/**
 *
 * @author Adrien
 */
public class Historique {
    private final BorderPane menu;
    private final SuiviDeFlotte sf;
    private StackPane root;
    private DatePicker datePicker;
    public Historique(){
        sf= new SuiviDeFlotte();
        menu=buildMenu();
        creer();
       
    }

     public BorderPane buildMenu(){
        BorderPane menu = new BorderPane();
        MenuBar menuBar = new MenuBar();
        // Mettre la barre de menu en haut 
            menu.setTop(menuBar); 
        // --- Menu Accueil
            Menu menuAccueil= new Menu("Accueil");
            MenuItem acc=new MenuItem("Accueil");     
            acc.setOnAction(this::Accueil);
            menuAccueil.getItems().addAll(acc);
        // --- Menu Gestion
            final Menu menuEdit = new Menu("Gestion");
            MenuItem boat = new MenuItem("Bateau");
            MenuItem pilot = new MenuItem("Pilote");
            MenuItem zone = new MenuItem("Zone");
            boat.setOnAction((ActionEvent e) -> {
                 System.out.println("affiche de la page bateau");
            });
            pilot.setOnAction((ActionEvent e) -> {
                 System.out.println("affiche de la page pilot");
            });
            zone.setOnAction((ActionEvent e) -> {
                 System.out.println("affiche de la page zone");
            });
            menuEdit.getItems().addAll(boat,pilot,zone);
        // --- Menu Alerte
            final Menu Alerte = new Menu("Alerte");
            MenuItem a= new MenuItem("Alerte SMS");
            a.setOnAction((ActionEvent e) -> {
                System.out.println("menu alerte click");
            });
            Alerte.getItems().addAll(a);
        
        // --- Menu Aide
            final Menu help = new Menu("Aide");
            help.setOnAction((ActionEvent e) -> {
                 System.out.println("menu aide click");
            });
        menuBar.getMenus().addAll(menuAccueil,menuEdit,Alerte,help);
        return menu;
    }
     public void Accueil(ActionEvent event) {
                // Doesn't go into this method on click of the menu
                    System.out.print("test");
                 }
    public void creer()  {
        root=new StackPane();
        BorderPane secondary=new BorderPane();
        BorderPane thirty=new BorderPane();
        // affiche la partie client
        
        // afiche la partie bateau
        
        // affiche la partie date
        VBox vbox = new VBox(200);
        datePicker = new DatePicker();
        vbox.getChildren().add(datePicker);
        secondary.setBottom(thirty);
        thirty.setRight(vbox);
        root.getChildren().add(secondary);
        root.getChildren().add(menu);
        
        
        Scene scene = new Scene(root, 1200, 800);      
        sf.setStage(scene);
        
    }
}
