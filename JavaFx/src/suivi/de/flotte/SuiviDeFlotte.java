/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package suivi.de.flotte;

import Modeles.Bateau;
import Modeles.Pilote;
import java.awt.Desktop;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.logging.*;
import javafx.application.*;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.*;
import javafx.geometry.Insets;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.image.*;
import javafx.scene.layout.*;
import javafx.stage.*;
import javax.imageio.ImageIO;
import base_de_donnees.DataBase;
import java.util.LinkedList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.cell.PropertyValueFactory;



/**
 *
 * @author Adrien
 */
public class SuiviDeFlotte extends Application {
   
    private ObservableList<Bateau> data=FXCollections.observableArrayList();
     private ObservableList<Bateau> data2=FXCollections.observableArrayList();
    private LinkedList<Bateau> lesBateaux=new LinkedList<Bateau>();
    private LinkedList<Bateau> lesBateaux2=new LinkedList<Bateau>();
    
    private ObservableList<Pilote> datap=FXCollections.observableArrayList();
    private ObservableList<Pilote> data2p=FXCollections.observableArrayList();
    private LinkedList<Pilote> lesPilotes=new LinkedList<Pilote>();
    private LinkedList<Pilote> lesPilotes2=new LinkedList<Pilote>();
    
    
    public DataBase db=new DataBase();
    private StackPane root;
    private BorderPane menu;
    final FileChooser fc = new FileChooser();
    private final Desktop desktop = Desktop.getDesktop();
    private Historique h;
    private Alerte alerte;
    private StackPane rootBateau;
    private StackPane rootBateauadd;
    private StackPane rootPeriph;
    private StackPane rootPeriphadd;
    private StackPane rootClient;
    private StackPane rootClientadd;
    private StackPane rootprincipale;
    private StackPane rootbas;
    private Scene scenePrincipale;
    private Scene sceneBateau;
    private Scene sceneBateauadd;
    private Scene scenePeriph;
    private Scene scenePeriphadd;
    private Scene sceneClient;
    private Scene sceneClientadd;
    private Scene scenebas;
    final FileChooser fileChooser = new FileChooser();
    Stage primaryStage;
    //Stage inter;
    ImageView map = new ImageView();
    Image img;
    BufferedImage bufferedImage;
    int nbBoat=0;
    HBox hb= new HBox();
   

    public SuiviDeFlotte() {
        this.menu = buildMenu();
        fc.setTitle("Load Map");
            fc.setInitialDirectory(new File(System.getProperty("user.home"))); 
            fc.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("PNG", "*.png"),
                new FileChooser.ExtensionFilter("JPG", "*.jpg"),
                new FileChooser.ExtensionFilter("TIFF", "*.tiff"),
                new FileChooser.ExtensionFilter("Bitmap", "*.bmp")
            );
        
    }
    public void loadMap(){
      // String pic = SuiviDeFlotte.class.getResource(url).toExternalForm();
      // root.setStyle("-fx-background-image: url('" + pic + "');");
       map.setImage(img);
       
       root.getChildren().add(map);
       root.getChildren().add(menu);
    }
    private void openFile(File file) {
        try {
            desktop.open(file);
        } catch (IOException ex) {
            System.out.println("Error:"+ex);
                
        }
    }
    public BorderPane buildMenu(){
        BorderPane menu = new BorderPane();
        MenuBar menuBar = new MenuBar();
        // Mettre la barre de menu en haut 
            menu.setTop(menuBar); 
        // --- Menu Accueil
             Menu menuAccueil= new Menu("Accueil");
            MenuItem load = new MenuItem("load");
            MenuItem refresh = new MenuItem("refresh");
            refresh.setOnAction((ActionEvent e) -> {
                
            });
            load.setOnAction(this::Load);

            
            menuAccueil.getItems().addAll(load,refresh);
             
        // --- Menu Gestion
          // --- Menu Gestion
            final Menu menuEdit = new Menu("Gestion");
            MenuItem boat = new MenuItem("Bateau");
            MenuItem pilot = new MenuItem("Pilote");
            MenuItem zone = new MenuItem("Zone");
            boat.setOnAction((ActionEvent e) -> {
                 final HBox hb = new HBox();
                rootBateau=new StackPane();             
                
                TableView tablea = new TableView();
                tablea.setEditable(true);
                TableView tablei = new TableView();
                tablei.setEditable(true);
                
                TableColumn actif = new TableColumn("Actif");
                actif.setMaxWidth(150);
                actif.setMinWidth(150);                           
                actif.setCellValueFactory(
                new PropertyValueFactory<Bateau, String>("nom"));
                
                TableColumn innactif = new TableColumn("Innactif");
                innactif.setMaxWidth(150);
                innactif.setMinWidth(150);
                innactif.setCellValueFactory(
                new PropertyValueFactory<Bateau, String>("nom"));
                
                tablea.getColumns().addAll(actif);
                tablea.setMaxWidth(300.0);
                tablei.getColumns().addAll(innactif);
                tablei.setMaxWidth(300.0);
                
                lesBateaux=db.getBateauActifDB();
                lesBateaux2=db.getBateauInactifDB();
                System.out.println(lesBateaux.size());
                for (Bateau b : lesBateaux){
                    System.out.println("coucou");
                    data.add(b);
                    System.out.println("coucou2");}
                 for (Bateau b : lesBateaux2){
                    System.out.println("coucou");
                    data2.add(b);
                    System.out.println("coucou2");}
                tablea.setItems(data);
                tablei.setItems(data2);
                
                Button ajouterb = new Button("Ajouter");
                ajouterb.setPrefSize(100, 20);
                Button ajouterp = new Button("Ajouter Périph");
                ajouterp.setPrefSize(150, 20);
                EvenementaddBateau even=new EvenementaddBateau(rootBateauadd,rootBateau,sceneBateauadd,sceneBateau,rootPeriphadd,rootPeriph,scenePeriphadd,scenePeriph,primaryStage,this);
                ajouterb.addEventHandler(ActionEvent.ACTION, even);
                EvenementaddPeriph even2=new EvenementaddPeriph(rootBateauadd,rootBateau,sceneBateauadd,sceneBateau,rootPeriphadd,rootPeriph,scenePeriphadd,scenePeriph,primaryStage,this);
                ajouterp.addEventHandler(ActionEvent.ACTION, even2);
                
               
                
                final VBox vbox = new VBox();
                vbox.setSpacing(100);
                final HBox hbox = new HBox();
                vbox.setSpacing(100);
                vbox.setPadding(new Insets(30, 0, 0, 30));
                hbox.getChildren().addAll(ajouterp,ajouterb);
                hb.getChildren().addAll(tablea,tablei);
                vbox.getChildren().addAll(hb,hbox);
                rootBateau.getChildren().addAll(buildMenu());
                rootBateau.getChildren().addAll(statusBar());
                rootBateau.getChildren().addAll(vbox);
                
                
                sceneBateau = new Scene(rootBateau, 1200, 800);
                primaryStage.setScene(sceneBateau);
                primaryStage.show();
                
                
            });
            pilot.setOnAction((ActionEvent e) -> {
                final HBox hb = new HBox();
                rootClient=new StackPane();             
                
                TableView tablea = new TableView();
                tablea.setEditable(true);
                TableView tablei = new TableView();
                tablei.setEditable(true);
                
                TableColumn actif = new TableColumn("Actif");
                actif.setMaxWidth(150);
                actif.setMinWidth(150);                           
                actif.setCellValueFactory(
                new PropertyValueFactory<Pilote, String>("nom"));
                TableColumn actifp = new TableColumn("");
                actifp.setMaxWidth(150);
                actifp.setMinWidth(150);                           
                actifp.setCellValueFactory(
                new PropertyValueFactory<Pilote, String>("prenom"));
                
                TableColumn innactif = new TableColumn("Innactif");
                innactif.setMaxWidth(150);
                innactif.setMinWidth(150);
                innactif.setCellValueFactory(
                new PropertyValueFactory<Pilote, String>("nom"));
                TableColumn innactifp = new TableColumn("");
                innactifp.setMaxWidth(150);
                innactifp.setMinWidth(150);
                innactifp.setCellValueFactory(
                new PropertyValueFactory<Pilote, String>("prenom"));
                
                tablea.getColumns().addAll(actif,actifp);
                tablea.setMaxWidth(300.0);
                tablei.getColumns().addAll(innactif,innactifp);
                tablei.setMaxWidth(300.0);
                
                lesPilotes=db.getPiloteActifDB();
                lesPilotes2=db.getPiloteInactifDB();
                
                for (Pilote p : lesPilotes){
                    
                    datap.add(p);
                    }
                 for (Pilote p : lesPilotes2){
                    
                    data2p.add(p);
                    }
                tablea.setItems(datap);
                tablei.setItems(data2p);
                
                Button ajouterb = new Button("Ajouter");
                ajouterb.setPrefSize(100, 20);
                
                EvenementaddClient even=new EvenementaddClient(rootClientadd,rootClient,sceneClientadd,sceneClient,primaryStage,this);
                ajouterb.addEventHandler(ActionEvent.ACTION, even);
                
               
                
                final VBox vbox = new VBox();
                vbox.setSpacing(100);
                vbox.setPadding(new Insets(30, 0, 0, 30));
                hb.getChildren().addAll(tablea,tablei);
                vbox.getChildren().addAll(hb,ajouterb);
                rootClient.getChildren().addAll(buildMenu());
                rootClient.getChildren().addAll(statusBar());
                rootClient.getChildren().addAll(vbox);
                
                
                sceneClient = new Scene(rootClient, 1200, 800);
                primaryStage.setScene(sceneClient);
                primaryStage.show();
            });
            zone.setOnAction((ActionEvent e) -> {
                 System.out.println("affiche de la page zone");
            });
            menuEdit.getItems().addAll(boat,pilot,zone);
        // --- Menu Alerte
            final Menu Alerte = new Menu("Alerte");
            MenuItem a= new MenuItem("Alerte SMS");
            a.setOnAction((ActionEvent e) -> {
                System.out.println("menu alerte click");
                alerte=new Alerte();
                primaryStage.close();
            });
            Alerte.getItems().addAll(a);
        // --- Menu Historique
           final Menu historique = new Menu("Historique"); 
           MenuItem hist= new MenuItem("Historique");
            hist.setOnAction((ActionEvent event) -> {
                System.out.println("menu historique click");
                h=new Historique();
                primaryStage.close();
        });
            historique.getItems().addAll(hist);
        // --- Menu Aide
            final Menu help = new Menu("Aide");
            MenuItem h=new MenuItem("Manuel");
            h.setOnAction((ActionEvent e) -> {
                 System.out.println("menu aide click");
                 OpenFile("Manuel.pdf");
            });
            help.getItems().add(h);
        menuBar.getMenus().addAll(menuAccueil,menuEdit,Alerte,historique,help);
        return menu;
    }
    public void Load(ActionEvent event) {
                 File file = fc.showOpenDialog(primaryStage);
                 
                    if (file != null) {
                     try {
                         bufferedImage = ImageIO.read(file);
                         img=SwingFXUtils.toFXImage(bufferedImage, null);
                         loadMap();
                     } catch (IOException ex) {
                         Logger.getLogger(SuiviDeFlotte.class.getName()).log(Level.SEVERE, null, ex);
                     }
                }
                }
    public BorderPane statusBar(){
        BorderPane statusBar=new BorderPane();
        statusBar.setStyle("color:white");
        Label bateau = new Label("bateau en mer: "+nbBoat);
        statusBar.setBottom(bateau);
        return statusBar;
    }
    @Override
    @SuppressWarnings("empty-statement")
    public void start(Stage primaryStage) {
        root = new StackPane();
        root.getChildren().addAll(statusBar());
        root.getChildren().addAll(buildMenu());
        
        Scene scene = new Scene(root, 1200, 800);
        
        primaryStage.setTitle("Suivi de flotte");
        primaryStage.setScene(scene);
        // charge le css
        scene.getStylesheets().add(SuiviDeFlotte.class.getResource("css/page.css").toExternalForm());
        // affiche la page
        primaryStage.show();
         primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
          public void handle(WindowEvent we) {
              System.out.println("Stage is closing");
              primaryStage.close();
          }
      });        
        this.primaryStage=primaryStage;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        TCPServer.lancer();
        launch(args);
        
    }

    BorderPane getMenu() {
        return menu;
    }
    Stage getStage() {
        return primaryStage;
    }
   void setStage(Scene s) {
        Stage stage=new Stage();
        stage.setTitle("Suivi de flotte");
        stage.setScene(s);
        // charge le css
        
        s.getStylesheets().add(SuiviDeFlotte.class.getResource("css/page.css").toExternalForm());
        // affiche la page
        stage.show();
        primaryStage=stage;
    }
     public static void OpenFile(String filename) {
	// On vérifie que la classe Desktop soit bien supportée :
	if ( Desktop.isDesktopSupported() ) {
	    // On récupère l'instance du desktop :
	    Desktop desktop = Desktop.getDesktop();
	    // On vérifie que la fonction open est bien supportée :
	    if (desktop.isSupported(Desktop.Action.OPEN)) {
 		// Et on lance l'application associé au fichier pour l'ouvrir :
		try{
		desktop.open(new File(filename));
		}
		catch(IOException e){}
	    }
	}
    }
}
