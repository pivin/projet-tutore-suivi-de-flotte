/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package suivi.de.flotte;

import Modeles.Bateau;
import Modeles.Peripherique;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import base_de_donnees.DataBase;
import java.sql.Date;
import java.util.LinkedList;
import javafx.scene.control.CheckBox;
import javafx.scene.control.cell.PropertyValueFactory;
/**
 *
 * @author romain
 */
public class EvenementaddBateau implements EventHandler<ActionEvent> {
    private DataBase db=new DataBase();
    private LinkedList<Peripherique> periphl=new LinkedList<Peripherique>();
    private ObservableList<String> periph =FXCollections.observableArrayList();
    
    
   private ObservableList<Bateau> data=FXCollections.observableArrayList();
   private ObservableList<Bateau> data2=FXCollections.observableArrayList();
   private LinkedList<Bateau> lesBateaux=new LinkedList<Bateau>();
   private LinkedList<Bateau> lesBateaux2=new LinkedList<Bateau>();
    private StackPane rootPeriphadd;
    private StackPane rootPeriph;
    private Scene scenePeriphadd;
    private Scene scenePeriph;
    private StackPane rootBateauadd;
    private StackPane rootBateau;
    private Scene sceneBateauadd;
    private Stage primaryStage;
    private Scene sceneBateau;
    private SuiviDeFlotte p;
    
  // ObservableList<Bateau> data,ObservableList<Bateau> data2,LinkedList<Bateau> lesBateaux,LinkedList<Bateau> lesBateaux2 
    public EvenementaddBateau(StackPane rba,StackPane rb,Scene sba,Scene b,StackPane rpa,StackPane rp,Scene spa,Scene pp,Stage ps,SuiviDeFlotte p){
        rootBateauadd=rba;
        rootBateau=rb;
        sceneBateauadd=sba;
        sceneBateau=b;
        rootPeriphadd=rpa;
        rootPeriph=rp;
        scenePeriphadd=spa;
        scenePeriph=pp;
        primaryStage=ps;
        
        this.p=p;
    }

        public void handle(ActionEvent me){
            
            final ComboBox comboBoxperiph = new ComboBox(periph);
            final VBox hbf = new VBox();
            final VBox hb = new VBox();
            final VBox hb2 = new VBox();
            final HBox hbc = new HBox();
            final HBox hbcombo = new HBox();
            final HBox hbcheck = new HBox();
            

            hbf.setPadding(new Insets(0, 0, 0, 0));
            hb.setPadding(new Insets(20, 0, 0, 175));
            hb2.setPadding(new Insets(20, 0, 0, 175));
            final TextField nom = new TextField("Nom");
            nom.setMaxWidth(200);
            
            final TextField immatriculation = new TextField("Immatriculation");
            immatriculation.setMaxWidth(200);
            
            final TextField type = new TextField("Type");
            type.setMaxWidth(200);
            
            final TextField date = new TextField("Date");
            date.setMaxWidth(200);
            
            final TextField etat = new TextField("Etat");
            etat.setMaxWidth(200);
            
                    
            final TextField capacite = new TextField("Capacité");
            capacite.setMaxWidth(200);
            
            final TextField poid = new TextField("Poid");
            poid.setMaxWidth(200);
            
            final TextField puissM = new TextField("Puissance Motrice");
            puissM.setMaxWidth(200);
            
            final CheckBox moteur =new CheckBox("Moteur");
            hbcheck.getChildren().addAll(moteur);
            hbcheck.setPadding(new Insets(10, 0, 0, 350));
            
            
           final Label label = new Label("Choix du périphérique");
           label.setPadding(new Insets(20, 0, 0, 340));
           periphl=db.getPeripheriqueInactifDB();
           System.out.println("OKKK"+periphl.size());
           for(Peripherique p : periphl){
               comboBoxperiph.getItems().add(p.getNumeroTel());
           }
           
           

            hb.getChildren().addAll(nom);
            hb.getChildren().addAll(immatriculation);
            hb.getChildren().addAll(type);
            hb.getChildren().addAll(date);
            hb.setSpacing(20);

            hb2.getChildren().addAll(etat);
            hb2.getChildren().addAll(capacite);
            hb2.getChildren().addAll(poid);
            hb2.getChildren().addAll(puissM);
            hb2.setSpacing(20);
            
            hbc.getChildren().addAll(hb,hb2);
            
            final HBox hbb = new HBox();
            hbb.setPadding(new Insets(20, 0, 0, 335));
            
            Button ajouter = new Button("Ajouter");
            ajouter.setOnAction((ActionEvent e1) -> {
                String d=date.getText();
                String[] dateInt=d.split("/");
                int jo=0;
                int m=0;
                int a=0;
                for(int j=0;j<dateInt.length;j++){
                    if(j==0){
                        jo=Integer.parseInt(dateInt[j]);
                    }
                    if(j==1){
                        m=Integer.parseInt(dateInt[j]);
                    }
                    if(j==2){
                        a=Integer.parseInt(dateInt[j]);
                    }
                    
                }
                Date dd=new Date(jo,m,a);
                
                
                db.ajouterBateau(nom.getText(),immatriculation.getText(),type.getText(),
                dd,etat.getText(),Integer.parseInt(capacite.getText()),
                Integer.parseInt(poid.getText()),Float.parseFloat(puissM.getText()),moteur.isSelected(),db.getPeripheriqueDB((String) comboBoxperiph.getValue()).get(0).getId());
                
                
                
                
                
                final HBox hbbb = new HBox();
                rootBateau=new StackPane();             
                
                TableView tablea = new TableView();
                tablea.setEditable(true);
                TableView tablei = new TableView();
                tablei.setEditable(true);
                
                TableColumn actif = new TableColumn("Actif");
                actif.setMaxWidth(150);
                actif.setMinWidth(150);                           
                actif.setCellValueFactory(
                new PropertyValueFactory<Bateau, String>("nom"));
                
                TableColumn innactif = new TableColumn("Innactif");
                innactif.setMaxWidth(150);
                innactif.setMinWidth(150);
                innactif.setCellValueFactory(
                new PropertyValueFactory<Bateau, String>("nom"));
                
                tablea.getColumns().addAll(actif);
                tablea.setMaxWidth(300.0);
                tablei.getColumns().addAll(innactif);
                tablei.setMaxWidth(300.0);
                
                lesBateaux=db.getBateauActifDB();
                lesBateaux2=db.getBateauInactifDB();
                System.out.println(lesBateaux.size());
                for (Bateau b : lesBateaux){
                    System.out.println("coucou");
                    data.add(b);
                    System.out.println("coucou2");}
                 for (Bateau b : lesBateaux2){
                    System.out.println("coucou");
                    data2.add(b);
                    System.out.println("coucou2");}
                tablea.setItems(data);
                tablei.setItems(data2);
                
                Button ajouterb = new Button("Ajouter");
                ajouterb.setPrefSize(100, 20);
                Button ajouterp = new Button("Ajouter Périph");
                ajouterp.setPrefSize(150, 20);
                EvenementaddBateau even=new EvenementaddBateau(rootBateauadd,rootBateau,sceneBateauadd,sceneBateau,rootPeriphadd,rootPeriph,scenePeriphadd,scenePeriph,primaryStage,p);
                ajouterb.addEventHandler(ActionEvent.ACTION, even);
                EvenementaddPeriph even2=new EvenementaddPeriph(rootBateauadd,rootBateau,sceneBateauadd,sceneBateau,rootPeriphadd,rootPeriph,scenePeriphadd,scenePeriph,primaryStage,p);
                ajouterp.addEventHandler(ActionEvent.ACTION, even2);
                
               
                
                final VBox vbox = new VBox();
                vbox.setSpacing(100);
                final HBox hbox = new HBox();
                vbox.setSpacing(100);
                vbox.setPadding(new Insets(30, 0, 0, 30));
                hbox.getChildren().addAll(ajouterp,ajouterb);
                hbbb.getChildren().addAll(tablea,tablei);
                vbox.getChildren().addAll(hbbb,hbox);
                rootBateau.getChildren().addAll(p.buildMenu());
                rootBateau.getChildren().addAll(p.statusBar());
                rootBateau.getChildren().addAll(vbox);
                
                
                sceneBateau = new Scene(rootBateau, 1200, 800);
                primaryStage.setScene(sceneBateau);
                primaryStage.show();
            });
                   
            Button annuler = new Button("Annuler");
            annuler.setOnAction((ActionEvent e2) -> {
                final HBox hbbb = new HBox();
                rootBateau=new StackPane();             
                
                TableView tablea = new TableView();
                tablea.setEditable(true);
                TableView tablei = new TableView();
                tablei.setEditable(true);
                
                TableColumn actif = new TableColumn("Actif");
                actif.setMaxWidth(150);
                actif.setMinWidth(150);                           
                actif.setCellValueFactory(
                new PropertyValueFactory<Bateau, String>("nom"));
                
                TableColumn innactif = new TableColumn("Innactif");
                innactif.setMaxWidth(150);
                innactif.setMinWidth(150);
                innactif.setCellValueFactory(
                new PropertyValueFactory<Bateau, String>("nom"));
                
                tablea.getColumns().addAll(actif);
                tablea.setMaxWidth(300.0);
                tablei.getColumns().addAll(innactif);
                tablei.setMaxWidth(300.0);
                
                lesBateaux=db.getBateauActifDB();
                lesBateaux2=db.getBateauInactifDB();
                System.out.println(lesBateaux.size());
                for (Bateau b : lesBateaux){
                    System.out.println("coucou");
                    data.add(b);
                    System.out.println("coucou2");}
                 for (Bateau b : lesBateaux2){
                    System.out.println("coucou");
                    data2.add(b);
                    System.out.println("coucou2");}
                tablea.setItems(data);
                tablei.setItems(data2);
                
                Button ajouterb = new Button("Ajouter");
                ajouterb.setPrefSize(100, 20);
                Button ajouterp = new Button("Ajouter Périph");
                ajouterp.setPrefSize(150, 20);
                EvenementaddBateau even=new EvenementaddBateau(rootBateauadd,rootBateau,sceneBateauadd,sceneBateau,rootPeriphadd,rootPeriph,scenePeriphadd,scenePeriph,primaryStage,p);
                ajouterb.addEventHandler(ActionEvent.ACTION, even);
                EvenementaddPeriph even2=new EvenementaddPeriph(rootBateauadd,rootBateau,sceneBateauadd,sceneBateau,rootPeriphadd,rootPeriph,scenePeriphadd,scenePeriph,primaryStage,p);
                ajouterp.addEventHandler(ActionEvent.ACTION, even2);
                
               
                
                final VBox vbox = new VBox();
                vbox.setSpacing(100);
                final HBox hbox = new HBox();
                vbox.setSpacing(100);
                vbox.setPadding(new Insets(30, 0, 0, 30));
                hbox.getChildren().addAll(ajouterp,ajouterb);
                hbbb.getChildren().addAll(tablea,tablei);
                vbox.getChildren().addAll(hbbb,hbox);
                rootBateau.getChildren().addAll(p.buildMenu());
                rootBateau.getChildren().addAll(p.statusBar());
                rootBateau.getChildren().addAll(vbox);
                
                
                sceneBateau = new Scene(rootBateau, 1200, 800);
                primaryStage.setScene(sceneBateau);
                primaryStage.show();
                
            });

            hbb.getChildren().addAll(ajouter,annuler);
            hbb.setSpacing(30);
            hbcombo.getChildren().addAll(comboBoxperiph);
            hbcombo.setPadding(new Insets(20, 0, 0, 350));
            hbf.getChildren().addAll(hbc,hbcheck,label);
            hbf.getChildren().addAll(hbcombo);
            hbf.getChildren().addAll(hbb);
            hbb.setSpacing(30);
           
            rootBateauadd=new StackPane();
            rootBateauadd.getChildren().add(hbf);
            sceneBateauadd=new Scene(rootBateauadd, 900, 500);
            primaryStage.setScene(sceneBateauadd);
            primaryStage.show();
        }
}
