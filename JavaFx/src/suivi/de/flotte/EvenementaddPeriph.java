/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package suivi.de.flotte;

import Modeles.Bateau;
import Modeles.Permi;
import base_de_donnees.DataBase;
import java.util.LinkedList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import base_de_donnees.DataBase;

/**
 *
 * @author romain
 */
public class EvenementaddPeriph implements EventHandler<ActionEvent> {
    private DataBase db=new DataBase();
    
    
    private ObservableList<Bateau> data=FXCollections.observableArrayList();
   private ObservableList<Bateau> data2=FXCollections.observableArrayList();
   private LinkedList<Bateau> lesBateaux=new LinkedList<Bateau>();
   private LinkedList<Bateau> lesBateaux2=new LinkedList<Bateau>();
   
    private StackPane rootBateauadd;
    private StackPane rootBateau;
    private Scene sceneBateauadd;
    private Scene sceneBateau;
    private StackPane rootPeriphadd;
    private StackPane rootPeriph;
    private Scene scenePeriphadd;
    private Stage primaryStage;
    private Scene scenePeriph;
    private SuiviDeFlotte p;
    public EvenementaddPeriph(StackPane rba,StackPane rb,Scene sba,Scene b,StackPane rpa,StackPane rp,Scene spa,Scene pp,Stage ps,SuiviDeFlotte p){
        rootBateauadd=rba;
        rootBateau=rb;
        sceneBateauadd=sba;
        sceneBateau=b;
        rootPeriphadd=rpa;
        rootPeriph=rp;
        scenePeriphadd=spa;
        scenePeriph=pp;
        primaryStage=ps;
        
        this.p=p;
    }

        public void handle(ActionEvent me){
                 
            final VBox hbf = new VBox();
            final VBox hb = new VBox();
            
            hbf.setPadding(new Insets(0, 0, 0, 0));
            hb.setPadding(new Insets(20, 0, 0, 175));
            final TextField fonction = new TextField("Fonction");
                    
            fonction.setMaxWidth(100);
            final TextField type = new TextField("Type");
                    
            type.setMaxWidth(100);
            final TextField numtel = new TextField("Numero de tel");
                    
            numtel.setMaxWidth(100);
            
            hb.getChildren().addAll(fonction);
            hb.getChildren().addAll(type);
            hb.getChildren().addAll(numtel);
            hb.setSpacing(20);
            
           

            final HBox hbb = new HBox();
            hbb.setPadding(new Insets(20, 0, 0, 150));
            
            Button ajouter = new Button("Ajouter");
            ajouter.setOnAction((ActionEvent e1) -> {
                db.ajouterPeripherique(fonction.getText(),type.getText(),numtel.getText());
                
                
                final HBox hbbb = new HBox();
                rootBateau=new StackPane();             
                
                TableView tablea = new TableView();
                tablea.setEditable(true);
                TableView tablei = new TableView();
                tablei.setEditable(true);
                
                TableColumn actif = new TableColumn("Actif");
                actif.setMaxWidth(150);
                actif.setMinWidth(150);                           
                actif.setCellValueFactory(
                new PropertyValueFactory<Bateau, String>("nom"));
                
                TableColumn innactif = new TableColumn("Innactif");
                innactif.setMaxWidth(150);
                innactif.setMinWidth(150);
                innactif.setCellValueFactory(
                new PropertyValueFactory<Bateau, String>("nom"));
                
                tablea.getColumns().addAll(actif);
                tablea.setMaxWidth(300.0);
                tablei.getColumns().addAll(innactif);
                tablei.setMaxWidth(300.0);
                
                lesBateaux=db.getBateauActifDB();
                lesBateaux2=db.getBateauInactifDB();
                System.out.println(lesBateaux.size());
                for (Bateau b : lesBateaux){
                    System.out.println("coucou");
                    data.add(b);
                    System.out.println("coucou2");}
                 for (Bateau b : lesBateaux2){
                    System.out.println("coucou");
                    data2.add(b);
                    System.out.println("coucou2");}
                tablea.setItems(data);
                tablei.setItems(data2);
                
                Button ajouterb = new Button("Ajouter");
                ajouterb.setPrefSize(100, 20);
                Button ajouterp = new Button("Ajouter Périph");
                ajouterp.setPrefSize(150, 20);
                EvenementaddBateau even=new EvenementaddBateau(rootBateauadd,rootBateau,sceneBateauadd,sceneBateau,rootPeriphadd,rootPeriph,scenePeriphadd,scenePeriph,primaryStage,p);
                ajouterb.addEventHandler(ActionEvent.ACTION, even);
                EvenementaddPeriph even2=new EvenementaddPeriph(rootBateauadd,rootBateau,sceneBateauadd,sceneBateau,rootPeriphadd,rootPeriph,scenePeriphadd,scenePeriph,primaryStage,p);
                ajouterp.addEventHandler(ActionEvent.ACTION, even2);
                
               
                
                final VBox vbox = new VBox();
                vbox.setSpacing(100);
                final HBox hbox = new HBox();
                vbox.setSpacing(100);
                vbox.setPadding(new Insets(30, 0, 0, 30));
                hbox.getChildren().addAll(ajouterp,ajouterb);
                hbbb.getChildren().addAll(tablea,tablei);
                vbox.getChildren().addAll(hbbb,hbox);
                rootBateau.getChildren().addAll(p.buildMenu());
                rootBateau.getChildren().addAll(p.statusBar());
                rootBateau.getChildren().addAll(vbox);
                
                
                sceneBateau = new Scene(rootBateau, 1200, 800);
                primaryStage.setScene(sceneBateau);
                primaryStage.show();
            });
                   
            Button annuler = new Button("Annuler");
            annuler.setOnAction((ActionEvent e2) -> {
                final HBox hbbb = new HBox();
                rootBateau=new StackPane();             
                
                TableView tablea = new TableView();
                tablea.setEditable(true);
                TableView tablei = new TableView();
                tablei.setEditable(true);
                
                TableColumn actif = new TableColumn("Actif");
                actif.setMaxWidth(150);
                actif.setMinWidth(150);                           
                actif.setCellValueFactory(
                new PropertyValueFactory<Bateau, String>("nom"));
                
                TableColumn innactif = new TableColumn("Innactif");
                innactif.setMaxWidth(150);
                innactif.setMinWidth(150);
                innactif.setCellValueFactory(
                new PropertyValueFactory<Bateau, String>("nom"));
                
                tablea.getColumns().addAll(actif);
                tablea.setMaxWidth(300.0);
                tablei.getColumns().addAll(innactif);
                tablei.setMaxWidth(300.0);
                
                lesBateaux=db.getBateauActifDB();
                lesBateaux2=db.getBateauInactifDB();
                System.out.println(lesBateaux.size());
                for (Bateau b : lesBateaux){
                    System.out.println("coucou");
                    data.add(b);
                    System.out.println("coucou2");}
                 for (Bateau b : lesBateaux2){
                    System.out.println("coucou");
                    data2.add(b);
                    System.out.println("coucou2");}
                tablea.setItems(data);
                tablei.setItems(data2);
                
                Button ajouterb = new Button("Ajouter");
                ajouterb.setPrefSize(100, 20);
                Button ajouterp = new Button("Ajouter Périph");
                ajouterp.setPrefSize(150, 20);
                EvenementaddBateau even=new EvenementaddBateau(rootBateauadd,rootBateau,sceneBateauadd,sceneBateau,rootPeriphadd,rootPeriph,scenePeriphadd,scenePeriph,primaryStage,p);
                ajouterb.addEventHandler(ActionEvent.ACTION, even);
                EvenementaddPeriph even2=new EvenementaddPeriph(rootBateauadd,rootBateau,sceneBateauadd,sceneBateau,rootPeriphadd,rootPeriph,scenePeriphadd,scenePeriph,primaryStage,p);
                ajouterp.addEventHandler(ActionEvent.ACTION, even2);
                
               
                
                final VBox vbox = new VBox();
                vbox.setSpacing(100);
                final HBox hbox = new HBox();
                vbox.setSpacing(100);
                vbox.setPadding(new Insets(30, 0, 0, 30));
                hbox.getChildren().addAll(ajouterp,ajouterb);
                hbbb.getChildren().addAll(tablea,tablei);
                vbox.getChildren().addAll(hbbb,hbox);
                rootBateau.getChildren().addAll(p.buildMenu());
                rootBateau.getChildren().addAll(p.statusBar());
                rootBateau.getChildren().addAll(vbox);
                
                
                sceneBateau = new Scene(rootBateau, 1200, 800);
                primaryStage.setScene(sceneBateau);
                primaryStage.show();
            });

            hbb.getChildren().addAll(ajouter,annuler);
            hbb.setSpacing(30);

            hbf.getChildren().addAll(hb);
            hbf.getChildren().addAll(hbb);
            hbb.setSpacing(30);
           
            rootPeriphadd=new StackPane();
            rootPeriphadd.getChildren().add(hbf);
            scenePeriphadd=new Scene(rootPeriphadd, 500, 350);
            primaryStage.setScene(scenePeriphadd);
            primaryStage.show();
        }
}
