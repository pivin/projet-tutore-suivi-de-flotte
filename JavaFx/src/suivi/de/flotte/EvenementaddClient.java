/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package suivi.de.flotte;

import Modeles.Permi;
import Modeles.Pilote;
import base_de_donnees.DataBase;
import java.sql.Date;
import java.util.LinkedList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author romain
 */
public class EvenementaddClient implements EventHandler<ActionEvent> {
    private DataBase db=new DataBase();
    private LinkedList<Permi> permil=new LinkedList<Permi>();
    private ObservableList<String> permi =FXCollections.observableArrayList();
    
    private ObservableList<Pilote> datap=FXCollections.observableArrayList();
    private ObservableList<Pilote> data2p=FXCollections.observableArrayList();
    private LinkedList<Pilote> lesPilotes=new LinkedList<Pilote>();
    private LinkedList<Pilote> lesPilotes2=new LinkedList<Pilote>();
    
    private StackPane rootClientadd;
    private StackPane rootClient;
    private Scene sceneClientadd;
    private Stage primaryStage;
    private Scene sceneClient;
    private SuiviDeFlotte p;
    public EvenementaddClient(StackPane rba,StackPane rb,Scene sba,Scene b,Stage ps,SuiviDeFlotte p){
        rootClientadd=rba;
        rootClient=rb;
        sceneClientadd=sba;
        sceneClient=b;
        primaryStage=ps;
        this.p=p;
    }

        public void handle(ActionEvent me){
            final ComboBox comboBoxpermi = new ComboBox(permi);
            
            final VBox hbf = new VBox();
            final VBox hb = new VBox();
            
            hbf.setPadding(new Insets(0, 0, 0, 0));
            hb.setPadding(new Insets(20, 0, 0, 175));
            final TextField nom = new TextField("Nom");
                    
            nom.setMaxWidth(100);
            final TextField prenom = new TextField("Prenom");
                    
            prenom.setMaxWidth(100);
            final TextField date = new TextField("Date");
                    
            date.setMaxWidth(100);
            final TextField numper = new TextField("Numero Perso");
                    
            numper.setMaxWidth(100);
            final TextField numurg = new TextField("Numero Urgence");
                    
            numurg.setMaxWidth(100);
            
             final Label label = new Label("Choix du périphérique");
           label.setPadding(new Insets(20, 0, 0, 340));
           
           permil=db.getPermiDB();
           
           for(Permi p : permil){
               comboBoxpermi.getItems().add(p.getType());
           }
            

            hb.getChildren().addAll(nom);
            hb.getChildren().addAll(prenom);
            hb.getChildren().addAll(date);
            hb.getChildren().addAll(numper);
            hb.getChildren().addAll(numurg);
            hb.getChildren().addAll(comboBoxpermi);
            hb.setSpacing(20);
            
           

            final HBox hbb = new HBox();
            hbb.setPadding(new Insets(20, 0, 0, 150));
            
            Button ajouter = new Button("Ajouter");
            ajouter.setOnAction((ActionEvent e1) -> {
                 String d=date.getText();
                String[] dateInt=d.split("/");
                int jo=0;
                int m=0;
                int a=0;
                for(int j=0;j<dateInt.length;j++){
                    if(j==0){
                        jo=Integer.parseInt(dateInt[j]);
                    }
                    if(j==1){
                        m=Integer.parseInt(dateInt[j]);
                    }
                    if(j==2){
                        a=Integer.parseInt(dateInt[j]);
                    }
                    
                }
                Date dd=new Date(jo,m,a);
                
                
                db.ajouterPilote(nom.getText(),prenom.getText(),dd,numper.getText(),numurg.getText(),db.getPermiDB((String) comboBoxpermi.getValue()).get(0).getId());
               
                final HBox hbbb = new HBox();
                rootClient=new StackPane();             
                
                TableView tablea = new TableView();
                tablea.setEditable(true);
                TableView tablei = new TableView();
                tablei.setEditable(true);
                
                TableColumn actif = new TableColumn("Actif");
                actif.setMaxWidth(150);
                actif.setMinWidth(150);                           
                actif.setCellValueFactory(
                new PropertyValueFactory<Pilote, String>("nom"));
                TableColumn actifp = new TableColumn("");
                actifp.setMaxWidth(150);
                actifp.setMinWidth(150);                           
                actifp.setCellValueFactory(
                new PropertyValueFactory<Pilote, String>("prenom"));
                
                TableColumn innactif = new TableColumn("Innactif");
                innactif.setMaxWidth(150);
                innactif.setMinWidth(150);
                innactif.setCellValueFactory(
                new PropertyValueFactory<Pilote, String>("nom"));
                TableColumn innactifp = new TableColumn("");
                innactifp.setMaxWidth(150);
                innactifp.setMinWidth(150);
                innactifp.setCellValueFactory(
                new PropertyValueFactory<Pilote, String>("prenom"));
                
                tablea.getColumns().addAll(actif,actifp);
                tablea.setMaxWidth(300.0);
                tablei.getColumns().addAll(innactif,innactifp);
                tablei.setMaxWidth(300.0);
                
                lesPilotes=db.getPiloteActifDB();
                lesPilotes2=db.getPiloteInactifDB();
                
                for (Pilote p : lesPilotes){
                    
                    datap.add(p);
                    }
                 for (Pilote p : lesPilotes2){
                    
                    data2p.add(p);
                    }
                tablea.setItems(datap);
                tablei.setItems(data2p);
                
                Button ajouterb = new Button("Ajouter");
                ajouterb.setPrefSize(100, 20);
                
                EvenementaddClient even=new EvenementaddClient(rootClientadd,rootClient,sceneClientadd,sceneClient,primaryStage,p);
                ajouterb.addEventHandler(ActionEvent.ACTION, even);
                
               
                
                final VBox vbox = new VBox();
                vbox.setSpacing(100);
                vbox.setPadding(new Insets(30, 0, 0, 30));
                hbbb.getChildren().addAll(tablea,tablei);
                vbox.getChildren().addAll(hbbb,ajouterb);
                rootClient.getChildren().addAll(p.buildMenu());
                rootClient.getChildren().addAll(p.statusBar());
                rootClient.getChildren().addAll(vbox);
                
                
                sceneClient = new Scene(rootClient, 1200, 800);
                primaryStage.setScene(sceneClient);
                primaryStage.show();
            });
                   
            Button annuler = new Button("Annuler");
            annuler.setOnAction((ActionEvent e2) -> {
                final HBox hbbb = new HBox();
                rootClient=new StackPane();             
                
                TableView tablea = new TableView();
                tablea.setEditable(true);
                TableView tablei = new TableView();
                tablei.setEditable(true);
                
                TableColumn actif = new TableColumn("Actif");
                actif.setMaxWidth(150);
                actif.setMinWidth(150);                           
                actif.setCellValueFactory(
                new PropertyValueFactory<Pilote, String>("nom"));
                TableColumn actifp = new TableColumn("");
                actifp.setMaxWidth(150);
                actifp.setMinWidth(150);                           
                actifp.setCellValueFactory(
                new PropertyValueFactory<Pilote, String>("prenom"));
                
                TableColumn innactif = new TableColumn("Innactif");
                innactif.setMaxWidth(150);
                innactif.setMinWidth(150);
                innactif.setCellValueFactory(
                new PropertyValueFactory<Pilote, String>("nom"));
                TableColumn innactifp = new TableColumn("");
                innactifp.setMaxWidth(150);
                innactifp.setMinWidth(150);
                innactifp.setCellValueFactory(
                new PropertyValueFactory<Pilote, String>("prenom"));
                
                tablea.getColumns().addAll(actif,actifp);
                tablea.setMaxWidth(300.0);
                tablei.getColumns().addAll(innactif,innactifp);
                tablei.setMaxWidth(300.0);
                
                lesPilotes=db.getPiloteActifDB();
                lesPilotes2=db.getPiloteInactifDB();
                
                for (Pilote p : lesPilotes){
                    
                    datap.add(p);
                    }
                 for (Pilote p : lesPilotes2){
                    
                    data2p.add(p);
                    }
                tablea.setItems(datap);
                tablei.setItems(data2p);
                
                Button ajouterb = new Button("Ajouter");
                ajouterb.setPrefSize(100, 20);
                
                EvenementaddClient even=new EvenementaddClient(rootClientadd,rootClient,sceneClientadd,sceneClient,primaryStage,p);
                ajouterb.addEventHandler(ActionEvent.ACTION, even);
                
               
                
                final VBox vbox = new VBox();
                vbox.setSpacing(100);
                vbox.setPadding(new Insets(30, 0, 0, 30));
                hbbb.getChildren().addAll(tablea,tablei);
                vbox.getChildren().addAll(hbbb,ajouterb);
                rootClient.getChildren().addAll(p.buildMenu());
                rootClient.getChildren().addAll(p.statusBar());
                rootClient.getChildren().addAll(vbox);
                
                
                sceneClient = new Scene(rootClient, 1200, 800);
                primaryStage.setScene(sceneClient);
                primaryStage.show();
            });

            hbb.getChildren().addAll(ajouter,annuler);
            hbb.setSpacing(30);

            hbf.getChildren().addAll(hb);
            hbf.getChildren().addAll(hbb);
            hbb.setSpacing(30);
           
            rootClientadd=new StackPane();
            rootClientadd.getChildren().add(hbf);
            sceneClientadd=new Scene(rootClientadd, 500, 350);
            primaryStage.setScene(sceneClientadd);
            primaryStage.show();
        }
}
