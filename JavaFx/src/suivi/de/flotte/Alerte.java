/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package suivi.de.flotte;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

/**
 *
 * @author Adrien
 */
public class Alerte{
 private final BorderPane menu;
    private final SuiviDeFlotte sf;
    private StackPane root;
    private DatePicker datePicker;
    public Alerte(){
        sf= new SuiviDeFlotte();
        menu=buildMenu();
        creer();
       
    }

     public BorderPane buildMenu(){
        BorderPane menu = new BorderPane();
        MenuBar menuBar = new MenuBar();
        // Mettre la barre de menu en haut 
            menu.setTop(menuBar); 
        // --- Menu Accueil
            Menu menuAccueil= new Menu("Accueil");
            MenuItem acc=new MenuItem("Accueil");     
            acc.setOnAction(this::Accueil);
            menuAccueil.getItems().addAll(acc);
        // --- Menu Gestion
            final Menu menuEdit = new Menu("Gestion");
            MenuItem boat = new MenuItem("Bateau");
            MenuItem pilot = new MenuItem("Pilote");
            MenuItem zone = new MenuItem("Zone");
            boat.setOnAction((ActionEvent e) -> {
                 System.out.println("affiche de la page bateau");
            });
            pilot.setOnAction((ActionEvent e) -> {
                 System.out.println("affiche de la page pilot");
            });
            zone.setOnAction((ActionEvent e) -> {
                 System.out.println("affiche de la page zone");
            });
            menuEdit.getItems().addAll(boat,pilot,zone);
          // --- Menu Historique
           final Menu historique = new Menu("Historique"); 
           MenuItem hist= new MenuItem("Historique");
            hist.setOnAction((ActionEvent event) -> {
                System.out.println("menu historique click");
                Historique h = new Historique();
                sf.primaryStage.close();
        });
            historique.getItems().addAll(hist);
        // --- Menu Aide
            final Menu help = new Menu("Aide");
            help.setOnAction((ActionEvent e) -> {
                 System.out.println("menu aide click");
            });
        menuBar.getMenus().addAll(menuAccueil,menuEdit,historique,help);
        return menu;
    }
     public void Accueil(ActionEvent event) {
                // Doesn't go into this method on click of the menu
                    System.out.print("test");
                 }
    public void creer()  {
        root=new StackPane();
        BorderPane bp=new BorderPane();BorderPane b=new BorderPane();
        HBox comboBox = new HBox(); HBox listBox = new HBox(); HBox controlBox = new HBox();
        ObservableList<String> msg = FXCollections.observableArrayList("Retour au cas ","Risque de colision");
        ComboBox cb = new ComboBox(msg); 
        Label comboSelection = new Label();
        cb.getSelectionModel().selectedItemProperty().addListener( new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov,  String old_val, String new_val) {  
                         comboSelection.setText(new_val);
            }
        });
        comboBox.getChildren().add(cb);
        comboBox.getChildren().add(comboSelection); 
        controlBox.getChildren().add(listBox);
        controlBox.getChildren().add(comboBox);
        TextField edit=new TextField("Message a ecrire");
        edit.setMaxSize(300,50);
        Button ajout= new Button("Valider");
        Button annuler = new Button("Annuler");
        b.setBottom(bp);
        bp.setLeft(ajout);
        bp.setRight(annuler);
        root.getChildren().add(edit);
        root.getChildren().add(menu);
        root.getChildren().add(b);
        Scene scene = new Scene(root, 1200, 800);      
        sf.setStage(scene);
        
    }
}
