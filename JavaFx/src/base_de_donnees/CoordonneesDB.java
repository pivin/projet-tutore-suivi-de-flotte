package base_de_donnees;

import java.sql.Date;
import Modeles.*;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;

public class CoordonneesDB {
	private ModeleDB modeleDB;
	private Statement statement;
	
	public CoordonneesDB(Connexion ConnexionBaseDeDonnee,ModeleDB modeleDB){
		this.modeleDB=modeleDB;
		statement =ConnexionBaseDeDonnee.makeStatement();
	}
	public void supprimerCoordonneesDB(){	
	String sql="drop table Coordonnees;";
	try {
		statement.executeUpdate(sql);
	} catch (SQLException e) {
		// TODO Auto-generated catch block
	}
	}
	public void ajouterCoordonnees(int idBateau,float longitude,float latitude,
			int idZone,Date dateHoraire){
		String sql="insert into Coordonnees(idBateau,longitude,latitude,idZone,dateHoraire) values(?,?,?,?,?)";
		try {
			PreparedStatement preparedStatement=modeleDB.getConnection().prepareStatement(sql);
			preparedStatement.setInt(1,idBateau);
			preparedStatement.setFloat(2,longitude);
			preparedStatement.setFloat(3,latitude);
			preparedStatement.setInt(4,idZone);
			preparedStatement.setDate(5,dateHoraire);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void ajouterCoordonnees(Coordonnees coordonnees){
		String sql="insert into Coordonnees(idBateau,longitude,latitude,idZone,dateHoraire) values(?,?,?,?,?)";
		try {
			PreparedStatement preparedStatement=modeleDB.getConnection().prepareStatement(sql);
			preparedStatement.setInt(1,coordonnees.getBateau().getId());
			preparedStatement.setFloat(2,coordonnees.getLongitude());
			preparedStatement.setFloat(3,coordonnees.getLatitude());
			preparedStatement.setInt(4,coordonnees.getZone().getId());
			preparedStatement.setDate(5,coordonnees.getDate());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public LinkedList<Coordonnees> getCoordonneesDB(Bateau bateau){
		ResultSet rs=null;
		ResultSet rsZone=null;
		Statement statement2=modeleDB.makeStatement();
		
		LinkedList<Coordonnees> lesCoordonnees=new LinkedList<Coordonnees>();
		try {
			rs = statement.executeQuery("SELECT * FROM Coordonnees where idBateau="+bateau.getId());
		while (rs.next()) {
			rsZone = statement2.executeQuery("SELECT * FROM Zone where id="+rs.getInt(1));
			System.out.println("boucle1");
			while (rsZone.next()) {
			Zone z=new Zone(rsZone.getInt(1),rsZone.getString(2),rsZone.getString(3),rsZone.getDate(4));
			lesCoordonnees.add(new Coordonnees(bateau,rs.getFloat(2),rs.getFloat(3),z,rs.getDate(5)));}}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lesCoordonnees;
		}
	
	
	
	public void creerCoordonneesDB(){
		
		String sql="create table Coordonnees("+
				"idBateau int(4)," +
				"longitude float(4)," +
				"latitude float(4),"+
				"idZone int(4),"+
				"dateHoraire date," +
				"foreign key(idBateau) references Bateau(id)," +
				"foreign key(idZone) references Zone(id),"+
				"Primary key (idBateau,dateHoraire));";
		try {
			statement.executeUpdate(sql);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	

}
