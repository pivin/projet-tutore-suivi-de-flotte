package base_de_donnees;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import Modeles.*;


public class ZoneDB {
	private ModeleDB modeleDB;
	private Statement statement;
	
	public ZoneDB(Connexion ConnexionBaseDeDonnee,ModeleDB modeleDB){
		this.modeleDB=modeleDB;
		statement =ConnexionBaseDeDonnee.makeStatement();
	}
	
	public void supprimerZoneDB(){
		String sql="drop table Zone;" ;
		
		try {
			statement.executeUpdate(sql);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
		}
		
	}
	public void ajouterZone(String nom,String type,Date dateCreation){
		String sql="insert into Zone(id,nom,type,dateCreation) values(?,?,?,?)";
		try {
			PreparedStatement preparedStatement=modeleDB.getConnection().prepareStatement(sql);
			preparedStatement.setInt(1,modeleDB.creerID("Zone"));
			preparedStatement.setString(2,nom);
			preparedStatement.setString(3,type);
			preparedStatement.setDate(4,dateCreation);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	
	
	public void ajouterZone(Zone zone){
		String sql="insert into Zone(id,nom,type,dateCreation) values(?,?,?,?)";
		try {
			PreparedStatement preparedStatement=modeleDB.getConnection().prepareStatement(sql);
			zone.setId(modeleDB.creerID("Zone"));
			preparedStatement.setInt(1,zone.getId());
			preparedStatement.setString(2,zone.getNom());
			preparedStatement.setString(3,zone.getType());
			preparedStatement.setDate(4,zone.getDate());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	public void creerZoneDB(){
		String sql="create table Zone("+
					"id int(4),"+
					"nom varchar(32),"+
					"type varchar(32),"+
					"dateCreation date," +
					"Primary Key(id));";
		
		try {
			statement.executeUpdate(sql);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	

}
