package base_de_donnees;
import Modeles.*;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;

public class PermiDB {
	private ModeleDB modeleDB;
	private Statement statement;
	
	public PermiDB(Connexion ConnexionBaseDeDonnee,ModeleDB modeleDB){
		this.modeleDB=modeleDB;
		statement =ConnexionBaseDeDonnee.makeStatement();
	}
	
	public void ajouterPermi(String type,Date dateValidite,String extension){
		String sql="insert into Permi(id,type,dateValidite,extension) values(?,?,?,?)";
		 try{
			 PreparedStatement preparedStatement=modeleDB.getConnection().prepareStatement(sql);
			 preparedStatement.setInt(1,modeleDB.creerID("Permi"));
			 preparedStatement.setString(2,type);
			 preparedStatement.setDate(3,dateValidite);
			 preparedStatement.setString(4,extension);
			 preparedStatement.executeUpdate();
		 
		 }
		 catch(SQLException e){
			 System.out.println("erreur");
		 }
		
		
	}
	public void ajouterPermi(Permi permi){
		String sql="insert into Permi(id,type,dateValidite,extension) values(?,?,?,?)";
	 try{
		 PreparedStatement preparedStatement=modeleDB.getConnection().prepareStatement(sql);
		 permi.setId(modeleDB.creerID("Permi"));
		 preparedStatement.setInt(1,permi.getId());
		 preparedStatement.setString(2,permi.getType());
		 preparedStatement.setDate(3,permi.getDate());
		 preparedStatement.setString(4,permi.getExtension());
		 
		 preparedStatement.executeUpdate();
	 
	 }
	 catch(SQLException e){
		 System.out.println("erreur");
	 }
	
	
}
	
	public void supprimerPermiDB(){
		String sql="drop table Permi;";
		try {
			statement.executeUpdate(sql);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
		}
		
		
	}
	
	public LinkedList<Permi> getPermiDB(){
		ResultSet rs=null;
		ResultSet rsPeri=null;
		Statement statement2=modeleDB.makeStatement();
		
		LinkedList<Permi> lesPermis=new LinkedList<Permi>();
		try {
			PreparedStatement prepareStatement=modeleDB.getConnection().prepareStatement("SELECT * FROM Permi");
			rs = prepareStatement.executeQuery();
		while (rs.next()) {
                    lesPermis.add(new Permi(rs.getInt(1),rs.getString(2),rs.getDate(3),rs.getString(4)));
			}
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lesPermis;
		}
	
	
	public LinkedList<Permi> getPermiDB(String type){
		ResultSet rs=null;
		ResultSet rsPeri=null;
		Statement statement2=modeleDB.makeStatement();
		
		LinkedList<Permi> lesPermis=new LinkedList<Permi>();
		try {
			PreparedStatement prepareStatement=modeleDB.getConnection().prepareStatement("SELECT * FROM Permi where type=?");
			prepareStatement.setString(1,type);
			rs = prepareStatement.executeQuery();
		while (rs.next()) {
                    lesPermis.add(new Permi(rs.getInt(1),rs.getString(2),rs.getDate(3),rs.getString(4)));
			}
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lesPermis;
		}
	
	public void creerPermiDB(){
		String sql="create table Permi("+
						"id int(4)not null,"+
						"type varchar(32),"+
						"dateValidite date,"+
						"extension varchar(32),"+
						"Primary key (id));";
						
		
		try {
			statement.executeUpdate(sql);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	

}
