package base_de_donnees;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class Connexion
{
	private Connection connexion_etablie;
	private Boolean connecte;
	
	// Autres parametres
	private String pilote_jdbc="com.mysql.jdbc.Driver"; // ="oracle.jdbc.driver.OracleDriver" (Oracle)
	// "jdbc:oracle:thin:@oracle:1521:oracle" (Oracle)
	// "jdbc:mysql://:/?user=&password=" (MySQL)
	private String u="utilisateur";
	private String mdp="u45";
	
	public Connexion()
	{
		System.out.println("Tentative de connexion.");
		try
		{
			Class.forName(pilote_jdbc);
			connexion_etablie=DriverManager.getConnection("jdbc:mysql://localhost/projet_flotte?user="+u+"&password="+mdp);
			connecte=true;
			System.out.println("Connecte.");
		}
		catch(SQLException e)
		{
			System.out.println("Echec de la connexion.\n"+e.getMessage()+e.getErrorCode());
		}
		catch(ClassNotFoundException e)
		{
			System.out.println("Echec du chargement du pilote JDBC."+e.getMessage());
		}
	}
	
	public void deconnexion()
	{
		if(connecte)
		{
			try
			{
				connexion_etablie.close();
				connecte=false;
			}
			catch(SQLException e)
			{
				System.out.println("Echec de la deconnexion.\n"+e.getMessage()+e.getErrorCode());
			}
		}
	}
	
	public Connection getConnection()
	{
		return connexion_etablie;
	}
	
	public Boolean est_connecte()
	{
		return connecte;
	}
	
	public Statement makeStatement()
	{ try{
		return this.connexion_etablie.createStatement();}
	 catch(SQLException e){return null;}
	}
	
	
	
	
	
}
