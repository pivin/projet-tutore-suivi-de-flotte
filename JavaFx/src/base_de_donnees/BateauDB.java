package base_de_donnees;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;

import Modeles.*;
public class BateauDB {
	private ModeleDB modeleDB;
	private Statement statement;
	
	public BateauDB(Connexion ConnexionBaseDeDonnee,ModeleDB modeleDB){
		this.modeleDB=modeleDB;
		statement =ConnexionBaseDeDonnee.makeStatement();
	}
	
	public void ajouterBateau(Bateau bateau){
		String sql="insert into Bateau(id,nom,immatriculation,type,anneeAchat,etat,capacite,poids,"+
			"puissanceMotrice,moteur,idPeri) values(?,?,?,?,?,?,?,?,?,?,?)";
		
		try {
			PreparedStatement preparedStatement=modeleDB.getConnection().prepareStatement(sql);
			bateau.setId(modeleDB.creerID("Bateau"));		
			preparedStatement.setInt(1,bateau.getId());
			preparedStatement.setString(2,bateau.getNom());
			preparedStatement.setString(3,bateau.getImmatriculation());
			preparedStatement.setString(4,bateau.getType());
			preparedStatement.setDate(5,bateau.getDate());
			preparedStatement.setString(6,bateau.getEtat());
			preparedStatement.setInt(7,bateau.getCapacite());
			preparedStatement.setInt(8,bateau.getPoids());
			preparedStatement.setFloat(9,bateau.getPuissanceMotrice());
			preparedStatement.setBoolean(10,bateau.getMoteur());
			preparedStatement.setInt(11,bateau.getPeri().getId());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
		
	}
	/*public LinkedList<Bateau> getBateauDB(String nom){
		ResultSet rs=null;
		
		LinkedList<Bateau> lesBateaux=new LinkedList<Bateau>();
		try {
			PreparedStatement preparedStatement =modeleDB.getConnection().prepareStatement("SELECT * FROM Bateau where nom= ?");
			preparedStatement.setString(1,nom);
			rs=preparedStatement.executeQuery();
		while (rs.next()) {
		
			
			lesBateaux.add(new Bateau(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getDate(5),rs.getString(6),rs.getInt(7),rs.getInt(8),rs.getFloat(9),rs.getBoolean(10),rs.getInt(11)));}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lesBateaux;
		}
	
	public LinkedList<Bateau> getBateauDB(){
		ResultSet rs=null;
		
		LinkedList<Bateau> lesBateaux=new LinkedList<Bateau>();
		try {
			PreparedStatement preparedStatement =modeleDB.getConnection().prepareStatement("SELECT * FROM Bateau");
			rs=preparedStatement.executeQuery();
		while (rs.next()) {
		
			
			lesBateaux.add(new Bateau(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getDate(5),rs.getString(6),rs.getInt(7),rs.getInt(8),rs.getFloat(9),rs.getBoolean(10),rs.getInt(11)));}
		
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lesBateaux;
		}*/
	
	
	public LinkedList<Bateau> getBateauDB(String nom){
		ResultSet rs=null;
		ResultSet rsPeri=null;
		Statement statement2=modeleDB.makeStatement();
		
		LinkedList<Bateau> lesBateaux=new LinkedList<Bateau>();
		try {
			PreparedStatement prepareStatement=modeleDB.getConnection().prepareStatement("SELECT * FROM Bateau where nom=?");
			prepareStatement.setString(1, nom);
			rs = prepareStatement.executeQuery();
		while (rs.next()) {
			rsPeri = statement2.executeQuery("SELECT * FROM Peripherique where id="+rs.getInt(11));
			System.out.println("boucle1");
			while (rsPeri.next()) {
			Peripherique z=new Peripherique(rsPeri.getInt(1),rsPeri.getString(2),rsPeri.getString(3),rsPeri.getString(4));
			lesBateaux.add(new Bateau(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getDate(5),rs.getString(6),rs.getInt(7),rs.getInt(8),rs.getFloat(9),rs.getBoolean(10),z));}
		}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lesBateaux;
		}

	
	public LinkedList<Bateau> getBateauDB(){
		ResultSet rs=null;
		ResultSet rsPeri=null;
		Statement statement2=modeleDB.makeStatement();
		
		LinkedList<Bateau> lesBateaux=new LinkedList<Bateau>();
		try {
			PreparedStatement prepareStatement=modeleDB.getConnection().prepareStatement("SELECT * FROM Bateau");
			rs = prepareStatement.executeQuery();
		while (rs.next()) {
			rsPeri = statement2.executeQuery("SELECT * FROM Peripherique where id="+rs.getInt(11));
			System.out.println("boucle1");
			while (rsPeri.next()) {
			Peripherique z=new Peripherique(rsPeri.getInt(1),rsPeri.getString(2),rsPeri.getString(3),rsPeri.getString(4));
			lesBateaux.add(new Bateau(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getDate(5),rs.getString(6),rs.getInt(7),rs.getInt(8),rs.getFloat(9),rs.getBoolean(10),z));}
		}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lesBateaux;
		}
	
	
	
	public void ajouterBateau(String nom,String immatriculation,String type,Date anneeAchat,String etat,
			int capacite,int poids,float puissanceMotrice,boolean moteur,int idPeri){
		String sql="insert into Bateau(id,nom,immatriculation,type,anneeAchat,etat,capacite,poids,"+
			"puissanceMotrice,moteur,idPeri) values(?,?,?,?,?,?,?,?,?,?,?)";
		
		try {
			PreparedStatement preparedStatement=modeleDB.getConnection().prepareStatement(sql);
			preparedStatement.setInt(1,modeleDB.creerID("Bateau"));
			preparedStatement.setString(2,nom);
			preparedStatement.setString(3,immatriculation);
			preparedStatement.setString(4,type);
			preparedStatement.setDate(5,anneeAchat);
			preparedStatement.setString(6,etat);
			preparedStatement.setInt(7,capacite);
			preparedStatement.setInt(8,poids);
			preparedStatement.setFloat(9,puissanceMotrice);
			preparedStatement.setBoolean(10,moteur);
			preparedStatement.setInt(11,idPeri);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
		
	}
	
	public void supprimerBateauDB(){
		String sql="drop table Bateau;";
		try {
			statement.executeUpdate(sql);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
		}
		
	}
	
	
	
	public void creerBateauDB(){

		String sql="create table Bateau("+
						"id int(4)not null,"+
						"nom varchar(32),"+
						"immatriculation varchar(32),"+
						"type varchar(32),"+
						"anneeAchat date,"+
						"etat varchar(32)," +
						"capacite int(20)," +
						"poids int(20)," +
						"puissanceMotrice float(20)," +
						"moteur boolean," +
						"idPeri int(4),"+	
						"primary key(id)," +
						"foreign key(idPeri) references Peripherique(id));";
		
		try {
			statement.executeUpdate(sql);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	

}
