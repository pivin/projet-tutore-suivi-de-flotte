package base_de_donnees;

import java.sql.*;
import Modeles.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;

public class PeripheriqueDB {
	private ModeleDB modeleDB;
	private Statement statement;
	
	public PeripheriqueDB(Connexion ConnexionBaseDeDonnee,ModeleDB modeleDB){
		this.modeleDB=modeleDB;
		statement =ConnexionBaseDeDonnee.makeStatement();
	}
	public void supprimerPeripheriqueDB(){
		String sql="drop table Peripherique;";
		try {
			statement.executeUpdate(sql);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
		}
		
	}

	
	
	
	//
	public void ajouterPeripherique(String fonction,String type, String numeroTel){
		String sql="insert into Peripherique(id,fonction,type,numeroTel) values(?,?,?,?)";
		try {
			PreparedStatement preparedStatement=modeleDB.getConnection().prepareStatement(sql);
			preparedStatement.setInt(1,modeleDB.creerID("Peripherique"));
			preparedStatement.setString(2, fonction);
			preparedStatement.setString(3, type);
			preparedStatement.setString(4,numeroTel);
			preparedStatement.executeUpdate();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	
		
	}
	public LinkedList<Peripherique> getPeripheriqueDB(String numero){
		ResultSet rs=null;
		ResultSet rsPeri=null;
		Statement statement2=modeleDB.makeStatement();
		
		LinkedList<Peripherique> lesPeripheriques=new LinkedList<Peripherique>();
		try {
			PreparedStatement prepareStatement=modeleDB.getConnection().prepareStatement("SELECT * FROM Peripherique where numeroTel=?");
			prepareStatement.setString(1,numero);
			rs = prepareStatement.executeQuery();
		while (rs.next()) {
                    lesPeripheriques.add(new Peripherique(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4)));
			}
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lesPeripheriques;
		}
	
	
	
	public LinkedList<Peripherique> getPeripheriqueDB(){
		ResultSet rs=null;
		ResultSet rsPeri=null;
		Statement statement2=modeleDB.makeStatement();
		
		LinkedList<Peripherique> lesPeripheriques=new LinkedList<Peripherique>();
		try {
			PreparedStatement prepareStatement=modeleDB.getConnection().prepareStatement("SELECT * FROM Peripherique");
			rs = prepareStatement.executeQuery();
		while (rs.next()) {
                    lesPeripheriques.add(new Peripherique(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4)));
			}
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lesPeripheriques;
		}
	
	public LinkedList<Peripherique> getPeripheriqueInactifDB(){
		ResultSet rs=null;
		ResultSet rsPeri=null;
		Statement statement2=modeleDB.makeStatement();
		
		LinkedList<Peripherique> lesPeripheriques=new LinkedList<Peripherique>();
		try {
			PreparedStatement prepareStatement=modeleDB.getConnection().prepareStatement("SELECT * FROM Peripherique where id not in(select idPeri from Bateau)");
			rs = prepareStatement.executeQuery();
		while (rs.next()) {
                    lesPeripheriques.add(new Peripherique(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4)));
			}
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lesPeripheriques;
		}
	public void ajouterPeripherique(Peripherique peripherique){
		String sql="insert into Peripherique(id,fonction,type,numeroTel) values(?,?,?,?)";
		try {
			PreparedStatement preparedStatement=modeleDB.getConnection().prepareStatement(sql);
			peripherique.setId(modeleDB.creerID("Peripherique"));
			preparedStatement.setInt(1,peripherique.getId());
			preparedStatement.setString(2, peripherique.getFonction());
			preparedStatement.setString(3, peripherique.getType());
			preparedStatement.setString(4,peripherique.getNumeroTel());
			preparedStatement.executeUpdate();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}}
	
		
	
	
	public void creerPeripheriqueDB(){
		String sql="create table Peripherique("+
						"id int(4)not null,"+
						"fonction varchar(32),"+
						"type varchar(32),"+
						"numeroTel varchar(32),"+
						"primary key(id));";
		
		try {
			statement.executeUpdate(sql);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	

	

}
