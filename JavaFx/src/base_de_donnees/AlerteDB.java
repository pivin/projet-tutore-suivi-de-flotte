package base_de_donnees;
import Modeles.*;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;

public class AlerteDB {
	private ModeleDB modeleDB;
	private Statement statement;
	
	public AlerteDB(Connexion ConnexionBaseDeDonnee,ModeleDB modeleDB){
		this.modeleDB=modeleDB;
		statement =ConnexionBaseDeDonnee.makeStatement();
	}
	
	public void supprimerAlerteDB(){
		String sql="drop table Alerte;";
		try {
			statement.executeUpdate(sql);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
		}
	}
	public void ajouterAlerte(String type,String message,int idExpediteur,int idDestinataire,
			Date dateEnvoi, Date dateDestination){
		String sql="insert into Alerte(id,type,message," +
				"idExpediteur,idDestinataire,dateEnvoi,dateDestination" +
				") values(?,?,?,?,?,?,?)";
		try {
			PreparedStatement preparedStatement=modeleDB.getConnection().prepareStatement(sql);
			preparedStatement.setInt(1,modeleDB.creerID("Alerte"));
			preparedStatement.setString(2,type);
			preparedStatement.setString(3,message);
			preparedStatement.setInt(4,idExpediteur);
			preparedStatement.setInt(5,idDestinataire);
			preparedStatement.setDate(6,dateEnvoi);
			preparedStatement.setDate(7,dateDestination);
			preparedStatement.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void ajouterAlerte(Alerte alerte){
		String sql="insert into Alerte(id,type,message," +
				"idExpediteur,idDestinataire,dateEnvoi,dateDestination" +
				") values(?,?,?,?,?,?,?)";
		try {
			PreparedStatement preparedStatement=modeleDB.getConnection().prepareStatement(sql);
			alerte.setId(modeleDB.creerID("Alerte"));
			preparedStatement.setInt(1,alerte.getId());
			preparedStatement.setString(2,alerte.getType());
			preparedStatement.setString(3,alerte.getMessage());
			preparedStatement.setInt(4,alerte.getExpediteur().getId());
			preparedStatement.setInt(5,alerte.getDestinataire().getId());
			preparedStatement.setDate(6,alerte.getDateEnvoi());
			preparedStatement.setDate(7,alerte.getDateDestinataire());
			preparedStatement.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public LinkedList<Alerte> getAlerteDB(Peripherique peripherique){
		ResultSet rs=null;
		ResultSet rsPeri=null;
		Statement statement2=modeleDB.makeStatement();

		
		LinkedList<Alerte> lesAlertes=new LinkedList<Alerte>();
		try {
			PreparedStatement preparedStatement=modeleDB.getConnection().prepareStatement("SELECT * FROM Alerte where idDestinataire=? order by dateEnvoi");
			preparedStatement.setInt(1,peripherique.getId());
			rs =preparedStatement.executeQuery();

			while (rs.next()) {
			rsPeri = statement2.executeQuery("SELECT * FROM Peripherique where id="+rs.getInt(4));
			System.out.println("boucle1");
			while (rsPeri.next()) {
			Peripherique p1=new Peripherique(rsPeri.getInt(1),rsPeri.getString(2),rsPeri.getString(3),rsPeri.getString(4));
			lesAlertes.add(new Alerte(rs.getInt(1),rs.getString(2),rs.getString(3),peripherique,p1,rs.getDate(6),rs.getDate(7)));}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lesAlertes;
		}
	
	public LinkedList<Alerte> getAlerteDB(){
		ResultSet rs=null;
		ResultSet rsPeri=null;
		ResultSet rsPeri2=null;
		Statement statement2=modeleDB.makeStatement();
		Statement statement3=modeleDB.makeStatement();
		Peripherique p1=null;
		Peripherique p2=null;
		
		LinkedList<Alerte> lesAlertes=new LinkedList<Alerte>();
		try {
			PreparedStatement preparedStatement=modeleDB.getConnection().prepareStatement("SELECT * FROM Alerte order by dateEnvoi");
			rs =preparedStatement.executeQuery();

			while (rs.next()) {
			rsPeri = statement2.executeQuery("SELECT * FROM Peripherique where id="+rs.getInt(4));
			rsPeri2= statement3.executeQuery("SELECT * FROM Peripherique where id="+rs.getInt(5));
			System.out.println("boucle1");
			while (rsPeri.next()) {
			p1=new Peripherique(rsPeri.getInt(1),rsPeri.getString(2),rsPeri.getString(3),rsPeri.getString(4));}
			while (rsPeri2.next()) {
			p2=new Peripherique(rsPeri2.getInt(1),rsPeri2.getString(2),rsPeri2.getString(3),rsPeri2.getString(4));}
			lesAlertes.add(new Alerte(rs.getInt(1),rs.getString(2),rs.getString(3),p2,p1,rs.getDate(6),rs.getDate(7)));}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lesAlertes;
		}
	
	public void creerAlerteDB(){
		 String sql="create table Alerte("+
						"id int(100),"+
						"type varchar(32),"+
						"message varchar(1000),"+
						"idExpediteur int(4),"+
						"idDestinataire int(4),"+
						"dateEnvoi Date,"+
						"dateDestination Date,"+
						"foreign key(idExpediteur) references Peripherique(id)," +
						"foreign key(idDestinataire) references Peripherique(id));";

		
		try {
			statement.executeUpdate(sql);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	

}
