package base_de_donnees;

import java.sql.Date;
import Modeles.*;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

public class PiloteDB {
	private ModeleDB modeleDB;
	private Statement statement;
	
	public PiloteDB(Connexion ConnexionBaseDeDonnee,ModeleDB modeleDB){
		this.modeleDB=modeleDB;
		statement =ConnexionBaseDeDonnee.makeStatement();
	}
	
	public LinkedList<Pilote> getPiloteDB(){
		ResultSet rs=null;
		ResultSet rsPermi=null;
		Statement statement2=modeleDB.makeStatement();
		
		LinkedList<Pilote> lesPilotes=new LinkedList<Pilote>();
		try {
			PreparedStatement preparedStatement=modeleDB.getConnection().prepareStatement("SELECT * FROM Pilote order by nom");
			rs = preparedStatement.executeQuery();
		while (rs.next()) {
			rsPermi = statement2.executeQuery("SELECT * FROM Permi where id="+rs.getInt(7));
			System.out.println("boucle1");
			while (rsPermi.next()) {
			Permi p=new Permi(rsPermi.getInt(1),rsPermi.getString(2),rsPermi.getDate(3),rsPermi.getString(4));
			lesPilotes.add(new Pilote(rs.getInt(1),rs.getString(2),
					rs.getString(3),rs.getDate(4),rs.getString(5),rs.getString(6),p));}}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lesPilotes;
	}

	public LinkedList<Pilote> getPiloteDB(String nom){
		ResultSet rs=null;
		ResultSet rsPermi=null;
		Statement statement2=modeleDB.makeStatement();
		
		LinkedList<Pilote> lesPilotes=new LinkedList<Pilote>();
		try {
			PreparedStatement preparedStatement=modeleDB.getConnection().prepareStatement("SELECT * FROM Pilote where nom=? order by nom");
			preparedStatement.setString(1,nom);
			rs = preparedStatement.executeQuery();
		while (rs.next()) {
			rsPermi = statement2.executeQuery("SELECT * FROM Permi where id="+rs.getInt(7));
			System.out.println("boucle1");
			while (rsPermi.next()) {
			Permi p=new Permi(rsPermi.getInt(1),rsPermi.getString(2),rsPermi.getDate(3),rsPermi.getString(4));
			lesPilotes.add(new Pilote(rs.getInt(1),rs.getString(2),
					rs.getString(3),rs.getDate(4),rs.getString(5),rs.getString(6),p));}}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lesPilotes;
		}
	
	public void ajouterPilote(String nom,String prenom,Date dateDiplome,String numeroPersonnel,
			String numeroUrgence,int idPermi){
		String sql="insert into Pilote(id,nom,prenom,dateDiplome,numeroPersonnel,numeroUrgence,idPermi) "+
			"values (?,?,?,?,?,?,?)";
		try {
			PreparedStatement preparedStatement=modeleDB.getConnection().prepareStatement(sql);
			preparedStatement.setInt(1,modeleDB.creerID("Pilote"));
			preparedStatement.setString(2,nom);
			preparedStatement.setString(3,prenom);
			preparedStatement.setDate(4,dateDiplome);
			preparedStatement.setString(5,numeroPersonnel);
			preparedStatement.setString(6,numeroUrgence);
			preparedStatement.setInt(7,idPermi);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
		
	}
	
	public void ajouterPilote(Pilote pilote){
		String sql="insert into Pilote(id,nom,prenom,dateDiplome,numeroPersonnel,numeroUrgence,idPermi) "+
			"values (?,?,?,?,?,?,?)";
		try {
			PreparedStatement preparedStatement=modeleDB.getConnection().prepareStatement(sql);
			pilote.setId(modeleDB.creerID("Pilote"));
			preparedStatement.setInt(1,pilote.getId());
			preparedStatement.setString(2,pilote.getNom());
			preparedStatement.setString(3,pilote.getPrenom());
			preparedStatement.setDate(4,pilote.getDate());
			preparedStatement.setString(5,pilote.getNumeroPersonnel());
			preparedStatement.setString(6,pilote.getNumeroUrgence());
			preparedStatement.setInt(7,pilote.getPermi().getId());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
		
	}
	
	
	public void supprimerPiloteDB(){
		String sql="drop table Pilote;";
		try {
			statement.executeUpdate(sql);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
		}
	}

	
	public void creerPiloteDB(){
		String sql="create table Pilote("+
						"id int(4)not null,"+
						"nom varchar(32),"+
						"prenom varchar(32),"+
						"dateDiplome date,"+
						"numeroPersonnel varchar(10),"+
						"numeroUrgence varchar(10)," +
						"idPermi int(4),"+
						"primary key(id),"+
						"unique(numeroPersonnel)," +
						"Foreign key(idPermi) references Permi(id));";
		
		try {
			statement.executeUpdate(sql);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	

}
