package base_de_donnees;

import java.sql.Date;
import java.util.LinkedList;
import Modeles.*;

import java.sql.Statement;

public class DataBase {
	private Connexion connexionDB;
	private AlerteDB alerteDB;
	private BateauDB bateauDB;
	private PeripheriqueDB peripheriqueDB;
	private PermiDB permiDB;
	private PiloteDB piloteDB;
	private PointDB pointDB;
	private CoordonneesDB coordonneesDB;
	private ZoneDB zoneDB;
	private UtilisationDB utilisationDB;
	private ModeleDB modeleDB;
	
	
	
	
	public DataBase(){
		connexionDB=new Connexion();
		modeleDB=new ModeleDB(connexionDB);
		Statement statement=connexionDB.makeStatement();
		alerteDB=new AlerteDB(connexionDB,modeleDB);
		bateauDB=new BateauDB(connexionDB,modeleDB);
		peripheriqueDB=new PeripheriqueDB(connexionDB,modeleDB);
		permiDB=new PermiDB(connexionDB,modeleDB);
		piloteDB=new PiloteDB(connexionDB,modeleDB);
		pointDB=new PointDB(connexionDB,modeleDB);
		coordonneesDB=new CoordonneesDB(connexionDB,modeleDB);
		zoneDB=new ZoneDB(connexionDB,modeleDB);
		utilisationDB=new UtilisationDB(connexionDB,modeleDB);
		}
	
public void SupprimerTables(){
	utilisationDB.supprimerUtilisationDB();
	alerteDB.supprimerAlerteDB();
	pointDB.supprimerPointDB();
	coordonneesDB.supprimerCoordonneesDB();
	zoneDB.supprimerZoneDB();
	bateauDB.supprimerBateauDB();
	piloteDB.supprimerPiloteDB();
	permiDB.supprimerPermiDB();
	peripheriqueDB.supprimerPeripheriqueDB();
	
}

public LinkedList<Coordonnees> getCoordonneesDB(Bateau bateau){
	return coordonneesDB.getCoordonneesDB(bateau);
}

public void initialisationTables(){
	SupprimerTables();
	permiDB.creerPermiDB();
	peripheriqueDB.creerPeripheriqueDB();
	alerteDB.creerAlerteDB();
	zoneDB.creerZoneDB();
	pointDB.creerPointDB();
	piloteDB.creerPiloteDB();
	bateauDB.creerBateauDB();
	utilisationDB.creerUtilisationDB();
	//peripheriqueDB.ajouterPeripherique();
	coordonneesDB.creerCoordonneesDB();}
	
public void ajouterPeripherique(String fonction,String type, String numeroTel){
	peripheriqueDB.ajouterPeripherique(fonction, type, numeroTel);}
	

public void ajouterPermi(String type,Date dateValidite,String extension){
	permiDB.ajouterPermi(type, dateValidite, extension);
}

public void ajouterPilote(String nom,String prenom,Date dateDiplome,String numeroPersonnel,
		String numeroUrgence,int idPermi){
	piloteDB.ajouterPilote(nom, prenom, dateDiplome, numeroPersonnel, numeroUrgence, idPermi);
	
}


public void ajouterBateau(String nom,String immatriculation,String type,Date anneeAchat,String etat,
		int capacite,int poids,float puissanceMotrice,boolean moteur,int idPeri){
	bateauDB.ajouterBateau(nom, immatriculation, type, anneeAchat, etat, capacite, poids, puissanceMotrice, moteur, idPeri);
}

public void ajouterUtilisation(Date dateU,int idPilote,int idBateau){
	utilisationDB.ajouterUtilisation(dateU, idPilote, idBateau);
}

public void ajouterZone(String nom,String type,Date dateCreation){
	zoneDB.ajouterZone(nom, type, dateCreation);
}

public void ajouterPoint(int abscisse,int ordonnee,int idZone){
	pointDB.ajouterPoint(abscisse, ordonnee, idZone);
}
	
public void ajouterCoordonnees(int idBateau,float longitude,float latitude,
		int idZone,Date dateHoraire){
	coordonneesDB.ajouterCoordonnees(idBateau, longitude, latitude, idZone, dateHoraire);
}

public void ajouterAlerte(String type,String message,int idExpediteur,int idDestinataire,
		Date dateEnvoi, Date dateDestination){
	alerteDB.ajouterAlerte(type, message, idExpediteur, idDestinataire, dateEnvoi, dateDestination);
}

public LinkedList<Bateau> getBateauDB(String nom){
	return bateauDB.getBateauDB(nom);
}
public LinkedList<Pilote> getPiloteDB(String nom){
	return piloteDB.getPiloteDB(nom);
	
}

public LinkedList<Pilote> getPiloteDB(){
	return piloteDB.getPiloteDB();
}

public LinkedList<Bateau> getBateauDB(){
	return bateauDB.getBateauDB();
	
}

public LinkedList<Alerte> getAlerteDB(Peripherique peripherique)
{return alerteDB.getAlerteDB(peripherique);}

public LinkedList<Utilisation> getUtilisationDB(){
	return utilisationDB.getUtilisationDB();
}
public LinkedList<Utilisation> getUtilisationDB(Bateau bateau){return utilisationDB.getUtilisationDB(bateau);}

public LinkedList<Utilisation> getUtilisationDB(Pilote pilote){return utilisationDB.getUtilisationDB(pilote);}
public LinkedList<Bateau> getBateauActifDB(){return utilisationDB.getBateauActifDB();}
public LinkedList<Bateau> getBateauInactifDB(){return utilisationDB.getBateauInactifDB();}
public LinkedList<Pilote> getPiloteActifDB(){return utilisationDB.getPiloteActifDB();}
public LinkedList<Pilote> getPiloteInactifDB(){return utilisationDB.getPiloteInactifDB();}
public LinkedList<Alerte> getAlerteDB(){return alerteDB.getAlerteDB();}
public LinkedList<Peripherique> getPeripheriqueDB(){return peripheriqueDB.getPeripheriqueDB();}
public LinkedList<Peripherique> getPeripheriqueInactifDB(){return peripheriqueDB.getPeripheriqueInactifDB();}
public LinkedList<Permi> getPermiDB(){return permiDB.getPermiDB();}
public LinkedList<Peripherique> getPeripheriqueDB(String numero){return peripheriqueDB.getPeripheriqueDB(numero);}
public LinkedList<Permi> getPermiDB(String type){return permiDB.getPermiDB(type);}

	public static void main(String [] args){
		DataBase db= new DataBase();
		db.initialisationTables();
		db.ajouterPeripherique("test45","essai45","45487845");
		Peripherique a=new Peripherique(1,"test45","essai45","45487845");
		db.ajouterPeripherique("test24","essai24","45487824");
		db.ajouterPermi("test",new Date(1995-12-25),"essai");
		
		db.ajouterPilote("Jean","Bon",new Date(1995-12-25),"06777877","068788888",1);
		db.ajouterPilote("Lea","Rang",new Date(1995-12-25),"06777437","068888888",1);
		db.ajouterPilote("Jean","Poche",new Date(1995-12-25),"06777277","068848888",1);
		db.ajouterPilote("Michael","Angelius",new Date(1995-12-25),"06779477","068888788",1);
		Bateau b=new Bateau(1,"Le bateau de Moué","AED5EF5EF5","voilier",new Date(1995-12-25)
		,"neuf",45,1580,450.5f,true,a);
		db.ajouterBateau("Le bateau de Moué","AED5EF5EF5","voilier",new Date(1995-12-25)
		,"neuf",45,1580,450.5f,true,1);
		db.ajouterBateau("Le bateau de Moué2","fafafafF5","voilier",new Date(1995-12-25)
		,"neuf",45,1580,450.5f,true,1);
		db.ajouterBateau("Le bateau de Moué3","AsvEF5","voilier",new Date(1995-12-25)
		,"neuf",45,1580,450.5f,true,1);
		db.ajouterBateau("Le bateau de Moué4","fzkfnzzF5EF5","voilier",new Date(1995-12-25)
		,"neuf",45,1580,450.5f,true,1);
		db.ajouterUtilisation(new Date(1995-12-25),1,1);
		db.ajouterZone("Triangle Bermudes","DangerMort",new Date(1995-12-25));
		db.ajouterPoint(3,2,1);
		db.ajouterCoordonnees(1,4.787f,8.4875f,1,new Date(1995,12,25));
		db.ajouterCoordonnees(1,3.548f,7.475f,1,new Date(1970,12,26));
		db.ajouterAlerte("urgence","Il n'y a plus de PQ",1,1,new Date(1995-12-25),new Date(1995-12-25));
		db.ajouterAlerte("urgence1","Le moteur est tombé à l'eau",1,1,new Date(1995-12-25),new Date(1995-12-25));
		Permi p=new Permi("Arnaque",new Date(1995-12-25),"Piege");
		Pilote p2=new Pilote(1,"Jean","Bon",new Date(1995-12-25),"06777877","068788888",p);
		db.ajouterUtilisation(new Date(17,12,2014),2,2);
		db.permiDB.ajouterPermi(p);
		LinkedList<Peripherique> l=db.getPeripheriqueInactifDB();
		System.out.println(l.size());
		for ( Peripherique k:l){
			System.out.println(k.getNumeroTel());
	
			
		}
		
		
	}
	




}



