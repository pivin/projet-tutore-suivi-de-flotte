package base_de_donnees;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;

import Modeles.*;

public class UtilisationDB {
	private ModeleDB modeleDB;
	private Statement statement;
	
	public UtilisationDB(Connexion ConnexionBaseDeDonnee,ModeleDB modeleDB){
		this.modeleDB=modeleDB;
		statement =ConnexionBaseDeDonnee.makeStatement();
	}
	public void supprimerUtilisationDB(){
		String sql="drop table Utilisation;";
		try {
			statement.executeUpdate(sql);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
		}
	}
	public LinkedList<Utilisation> getUtilisationDB(Pilote pilote){
		ResultSet rs=null;
		ResultSet rsPilote=null;
		ResultSet rsBateau=null;
		ResultSet rsPeri=null;
		ResultSet rsPermi=null;
		Statement statement2=modeleDB.makeStatement();
		Statement statement3=modeleDB.makeStatement();
		Statement statement4=modeleDB.makeStatement();
		Statement statement5=modeleDB.makeStatement();
		Pilote p1=null;
		Peripherique p2=null;
		Bateau b=null;
		Permi p3=null;
		
		LinkedList<Utilisation> lesUtilisations=new LinkedList<Utilisation>();
		try {
			PreparedStatement preparedStatement=modeleDB.getConnection().prepareStatement("SELECT * FROM Utilisation where idPilote=? order by dateU");
			preparedStatement.setInt(1,pilote.getId());
			rs =preparedStatement.executeQuery();

			while (rs.next()) {
			rsPilote = statement2.executeQuery("SELECT * FROM Pilote where id="+rs.getInt(2));
			rsBateau= statement3.executeQuery("SELECT * FROM Bateau where id="+rs.getInt(3));
			
			while (rsPilote.next()) {
				
				rsPermi = statement4.executeQuery("SELECT * FROM Permi where id="+rsPilote.getInt(7));
				System.out.println("boucle1");
				while(rsPermi.next()){
					p3=new Permi(rsPermi.getInt(1),rsPermi.getString(2),rsPermi.getDate(3),rsPermi.getString(4));
					System.out.println(p3.getType());
					
					
				}
			p1=new Pilote(rsPilote.getInt(1),rsPilote.getString(2),rsPilote.getString(3),rsPilote.getDate(4),rsPilote.getString(5),rsPilote.getString(6),p3);
			
			}
			
			while (rsBateau.next()) {
				
				rsPeri = statement4.executeQuery("SELECT * FROM Peripherique where id="+rsBateau.getInt(11));
				System.out.println("boucle2");
				while(rsPeri.next()){System.out.println("boucle22");
					
					p2=new Peripherique(rsPeri.getInt(1),rsPeri.getString(2),rsPeri.getString(3),rsPeri.getString(4));
					}
				b=new Bateau(rsBateau.getInt(1),rsBateau.getString(2),rsBateau.getString(3),rsBateau.getString(4),rsBateau.getDate(5),rsBateau.getString(6),rsBateau.getInt(7),rsBateau.getInt(8),rsBateau.getFloat(9),rsBateau.getBoolean(10),p2);
			}
			
			lesUtilisations.add(new Utilisation(rs.getDate(1),p1,b));}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lesUtilisations;
		}
	
	public LinkedList<Utilisation> getUtilisationDB(Bateau bateau){
		ResultSet rs=null;
		ResultSet rsPilote=null;
		ResultSet rsBateau=null;
		ResultSet rsPeri=null;
		ResultSet rsPermi=null;
		Statement statement2=modeleDB.makeStatement();
		Statement statement3=modeleDB.makeStatement();
		Statement statement4=modeleDB.makeStatement();
		Statement statement5=modeleDB.makeStatement();
		Pilote p1=null;
		Peripherique p2=null;
		Bateau b=null;
		Permi p3=null;
		
		LinkedList<Utilisation> lesUtilisations=new LinkedList<Utilisation>();
		try {
			PreparedStatement preparedStatement=modeleDB.getConnection().prepareStatement("SELECT * FROM Utilisation where idBateau=? order by dateU");
			preparedStatement.setInt(1,bateau.getId());
			rs =preparedStatement.executeQuery();

			while (rs.next()) {
			rsPilote = statement2.executeQuery("SELECT * FROM Pilote where id="+rs.getInt(2));
			rsBateau= statement3.executeQuery("SELECT * FROM Bateau where id="+rs.getInt(3));
			
			while (rsPilote.next()) {
				
				rsPermi = statement4.executeQuery("SELECT * FROM Permi where id="+rsPilote.getInt(7));
				System.out.println("boucle1");
				while(rsPermi.next()){
					p3=new Permi(rsPermi.getInt(1),rsPermi.getString(2),rsPermi.getDate(3),rsPermi.getString(4));
					System.out.println(p3.getType());
					
					
				}
			p1=new Pilote(rsPilote.getInt(1),rsPilote.getString(2),rsPilote.getString(3),rsPilote.getDate(4),rsPilote.getString(5),rsPilote.getString(6),p3);
			
			}
			
			while (rsBateau.next()) {
				
				rsPeri = statement4.executeQuery("SELECT * FROM Peripherique where id="+rsBateau.getInt(11));
				System.out.println("boucle2");
				while(rsPeri.next()){System.out.println("boucle22");
					
					p2=new Peripherique(rsPeri.getInt(1),rsPeri.getString(2),rsPeri.getString(3),rsPeri.getString(4));
					}
				b=new Bateau(rsBateau.getInt(1),rsBateau.getString(2),rsBateau.getString(3),rsBateau.getString(4),rsBateau.getDate(5),rsBateau.getString(6),rsBateau.getInt(7),rsBateau.getInt(8),rsBateau.getFloat(9),rsBateau.getBoolean(10),p2);
			}
			
			lesUtilisations.add(new Utilisation(rs.getDate(1),p1,b));}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lesUtilisations;
		}
	
        public LinkedList<Bateau> getBateauActifDB(){
		ResultSet rs=null;
		ResultSet rsPilote=null;
		ResultSet rsBateau=null;
		ResultSet rsPeri=null;
		ResultSet rsPermi=null;
		Statement statement2=modeleDB.makeStatement();
		Statement statement3=modeleDB.makeStatement();
		Statement statement4=modeleDB.makeStatement();
		Statement statement5=modeleDB.makeStatement();
		Peripherique p2=null;
		Bateau b=null;
		
		LinkedList<Bateau> lesBateaux=new LinkedList<Bateau>();
		try {
			PreparedStatement preparedStatement=modeleDB.getConnection().prepareStatement("SELECT * FROM Utilisation order by dateU");
			rs =preparedStatement.executeQuery();

			while (rs.next()) {
			rsBateau= statement3.executeQuery("SELECT * FROM Bateau where id="+rs.getInt(3));
			

			
			while (rsBateau.next()) {
				
				rsPeri = statement4.executeQuery("SELECT * FROM Peripherique where id="+rsBateau.getInt(11));
				System.out.println("boucle2");
				while(rsPeri.next()){System.out.println("boucle22");
					
					p2=new Peripherique(rsPeri.getInt(1),rsPeri.getString(2),rsPeri.getString(3),rsPeri.getString(4));
					}
				lesBateaux.add(new Bateau(rsBateau.getInt(1),rsBateau.getString(2),rsBateau.getString(3),rsBateau.getString(4),rsBateau.getDate(5),rsBateau.getString(6),rsBateau.getInt(7),rsBateau.getInt(8),rsBateau.getFloat(9),rsBateau.getBoolean(10),p2));
			}
			
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lesBateaux;
		}
	
        
        
        
    			
	
	public LinkedList<Utilisation> getUtilisationDB(){
		ResultSet rs=null;
		ResultSet rsPilote=null;
		ResultSet rsBateau=null;
		ResultSet rsPeri=null;
		ResultSet rsPermi=null;
		Statement statement2=modeleDB.makeStatement();
		Statement statement3=modeleDB.makeStatement();
		Statement statement4=modeleDB.makeStatement();
		Statement statement5=modeleDB.makeStatement();
		Pilote p1=null;
		Peripherique p2=null;
		Bateau b=null;
		Permi p3=null;
		
		LinkedList<Utilisation> lesUtilisations=new LinkedList<Utilisation>();
		try {
			PreparedStatement preparedStatement=modeleDB.getConnection().prepareStatement("select * from Bateau /SELECT * FROM Utilisation order by dateU");
			rs =preparedStatement.executeQuery();

			while (rs.next()) {
			rsPilote = statement2.executeQuery("SELECT * FROM Pilote where id="+rs.getInt(2));
			rsBateau= statement3.executeQuery("SELECT * FROM Bateau where id="+rs.getInt(3));
			
			while (rsPilote.next()) {
				
				rsPermi = statement4.executeQuery("SELECT * FROM Permi where id="+rsPilote.getInt(7));
				System.out.println("boucle1");
				while(rsPermi.next()){
					p3=new Permi(rsPermi.getInt(1),rsPermi.getString(2),rsPermi.getDate(3),rsPermi.getString(4));
					System.out.println(p3.getType());
					
					
				}
			p1=new Pilote(rsPilote.getInt(1),rsPilote.getString(2),rsPilote.getString(3),rsPilote.getDate(4),rsPilote.getString(5),rsPilote.getString(6),p3);
			
			}
			
			while (rsBateau.next()) {
				
				rsPeri = statement4.executeQuery("SELECT * FROM Peripherique where id="+rsBateau.getInt(11));
				System.out.println("boucle2");
				while(rsPeri.next()){System.out.println("boucle22");
					
					p2=new Peripherique(rsPeri.getInt(1),rsPeri.getString(2),rsPeri.getString(3),rsPeri.getString(4));
					}
				b=new Bateau(rsBateau.getInt(1),rsBateau.getString(2),rsBateau.getString(3),rsBateau.getString(4),rsBateau.getDate(5),rsBateau.getString(6),rsBateau.getInt(7),rsBateau.getInt(8),rsBateau.getFloat(9),rsBateau.getBoolean(10),p2);
			}
			
			lesUtilisations.add(new Utilisation(rs.getDate(1),p1,b));}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lesUtilisations;
		}
	
	
	
	public void ajouterUtilisation(Date dateU,int idPilote,int idBateau){
		String sql="insert into Utilisation(dateU,idPilote,idBateau) values(?,?,?)";
		try {
			PreparedStatement preparedStatement=modeleDB.getConnection().prepareStatement(sql);
			preparedStatement.setDate(1,dateU);
			preparedStatement.setInt(2,idPilote);
			preparedStatement.setInt(3,idBateau);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	
	public LinkedList<Pilote> getPiloteInactifDB(){
		ResultSet rs=null;
		ResultSet rsPermi=null;
		Statement statement2=modeleDB.makeStatement();
		
		LinkedList<Pilote> lesPilotes=new LinkedList<Pilote>();
		try {
			PreparedStatement prepareStatement=modeleDB.getConnection().prepareStatement("SELECT * FROM Pilote where id not in (select idPilote from Utilisation)");
			rs = prepareStatement.executeQuery();
		while (rs.next()) {
			rsPermi = statement2.executeQuery("SELECT * FROM Permi where id="+rs.getInt(7));
			System.out.println("boucle1");
			while (rsPermi.next()) {
				Permi p=new Permi(rsPermi.getInt(1),rsPermi.getString(2),rsPermi.getDate(3),rsPermi.getString(4));
				lesPilotes.add(new Pilote(rs.getInt(1),rs.getString(2),
						rs.getString(3),rs.getDate(4),rs.getString(5),rs.getString(6),p));}
			
		}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lesPilotes;
		}
	public LinkedList<Bateau> getBateauInactifDB(){
		ResultSet rs=null;
		ResultSet rsPeri=null;
		Statement statement2=modeleDB.makeStatement();
		
		LinkedList<Bateau> lesBateaux=new LinkedList<Bateau>();
		try {
			PreparedStatement prepareStatement=modeleDB.getConnection().prepareStatement("SELECT * FROM Bateau where id not in (select idBateau from Utilisation)");
			rs = prepareStatement.executeQuery();
		while (rs.next()) {
			rsPeri = statement2.executeQuery("SELECT * FROM Peripherique where id="+rs.getInt(11));
			System.out.println("boucle1");
			while (rsPeri.next()) {
			Peripherique z=new Peripherique(rsPeri.getInt(1),rsPeri.getString(2),rsPeri.getString(3),rsPeri.getString(4));
			lesBateaux.add(new Bateau(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getDate(5),rs.getString(6),rs.getInt(7),rs.getInt(8),rs.getFloat(9),rs.getBoolean(10),z));}
		}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lesBateaux;
		}
	
	public LinkedList<Pilote> getPiloteActifDB(){
		ResultSet rs=null;
		ResultSet rsPermi=null;
		Statement statement2=modeleDB.makeStatement();
		
		LinkedList<Pilote> lesPilotes=new LinkedList<Pilote>();
		try {
			PreparedStatement prepareStatement=modeleDB.getConnection().prepareStatement("SELECT * FROM Pilote where id in (select idPilote from Utilisation)");
			rs = prepareStatement.executeQuery();
		while (rs.next()) {
			rsPermi = statement2.executeQuery("SELECT * FROM Permi where id="+rs.getInt(7));
			System.out.println("boucle1");
			while (rsPermi.next()) {
				Permi p=new Permi(rsPermi.getInt(1),rsPermi.getString(2),rsPermi.getDate(3),rsPermi.getString(4));
				lesPilotes.add(new Pilote(rs.getInt(1),rs.getString(2),
						rs.getString(3),rs.getDate(4),rs.getString(5),rs.getString(6),p));}
			
		}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lesPilotes;
		}
	
	
	public void ajouterUtilisation(Utilisation utilisation){
		String sql="insert into Utilisation(dateU,idPilote,idBateau) values(?,?,?)";
		try {
			PreparedStatement preparedStatement=modeleDB.getConnection().prepareStatement(sql);
			preparedStatement.setDate(1,utilisation.getDate());
			preparedStatement.setInt(2,utilisation.getPilote().getId());
			preparedStatement.setInt(3,utilisation.getBateau().getId());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	
	
	public void creerUtilisationDB(){
		String sql="create table Utilisation("+
						"dateU date,"+
						"idPilote int(4)," +
						"idBateau int(4),"+
						"foreign key(idPilote) references Pilote(id),"+
						"foreign key(idBateau) references Bateau(id));";
		
		try {
			statement.executeUpdate(sql);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	

}
