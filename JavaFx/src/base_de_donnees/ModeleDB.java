package base_de_donnees;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ModeleDB {
	private Connexion ConnexionBaseDeDonnee;
	private Statement statement;
	
	public ModeleDB(Connexion ConnexionBaseDeDonnee){
		this.ConnexionBaseDeDonnee=ConnexionBaseDeDonnee;
		statement =ConnexionBaseDeDonnee.makeStatement();
	}
	public Connection getConnection(){
		return ConnexionBaseDeDonnee.getConnection();
		
	}
	
	public Statement makeStatement(){
		return ConnexionBaseDeDonnee.makeStatement();}
	

	public int creerID(String Table){
	int max=0;
	ResultSet resultat=null;
	try {
		 resultat = statement.executeQuery( "SELECT max(id) from "+Table+";" );
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	 if (resultat != null) {
         try {
			resultat.next();
			max=resultat.getInt(1);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}}
	return max+1; 
	}
	
	

	
	
	
	

}
