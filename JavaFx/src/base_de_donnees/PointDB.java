package base_de_donnees;

import java.sql.PreparedStatement;
import Modeles.*;
import java.sql.SQLException;
import java.sql.Statement;

public class PointDB {
	private ModeleDB modeleDB;
	private Statement statement;
	
	public PointDB(Connexion ConnexionBaseDeDonnee,ModeleDB modeleDB){
		this.modeleDB=modeleDB;
		statement =ConnexionBaseDeDonnee.makeStatement();
	}
	public void supprimerPointDB(){
		String sql="drop table Point;";
		try {
			statement.executeUpdate(sql);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
		}
		
	}
	public void ajouterPoint(int abscisse,int ordonnee,int idZone){
		String sql="insert into Point(id,abscisse,ordonnee,idZone) values(?,?,?,?)";
		try {
			PreparedStatement preparedStatement=modeleDB.getConnection().prepareStatement(sql);
			preparedStatement.setInt(1,modeleDB.creerID("Point"));
			preparedStatement.setInt(2,abscisse);
			preparedStatement.setInt(3,ordonnee);
			preparedStatement.setInt(4,idZone);
			preparedStatement.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void ajouterPoint(Point point){
		String sql="insert into Point(id,abscisse,ordonnee,idZone) values(?,?,?,?)";
		try {
			PreparedStatement preparedStatement=modeleDB.getConnection().prepareStatement(sql);
			point.setId(modeleDB.creerID("Point"));
			preparedStatement.setInt(1,point.getId());
			preparedStatement.setInt(2,point.getAbscisse());
			preparedStatement.setInt(3,point.getOrdonnee());
			preparedStatement.setInt(4,point.getZone().getId());
			preparedStatement.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	public void creerPointDB(){
		 String sql="create table Point("+
				 	"id int(6),"+
					"abscisse int(32),"+
					"ordonnee int(32),"+
					"idZone int(4)," +
					"Primary Key (id)," +
					"Foreign Key (idZone) references Zone(id));";
					
		try {
			statement.executeUpdate(sql);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	

	

}
