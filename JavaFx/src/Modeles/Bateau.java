package Modeles;

import java.sql.Date;

public class Bateau {
	private int id;
	private String nom;
	private String immatriculation;
	private String type;
	private Date date;
	private String etat;
	private int capacite;
	private int poids;
	private float puissanceMotrice;
	private boolean moteur;
	private Peripherique peri;
	
	public Bateau(int id,String nom, String immatriculation,String type,Date date, 
			String etat, int capacite, int poids, 
			float puissanceMotrice,boolean moteur,Peripherique peri){
		this.id=id;
		this.nom=nom;
		this.immatriculation=immatriculation;
		this.type=type;
		this.date=date;
		this.etat=etat;
		this.capacite=capacite;
		this.poids=poids;
		this.puissanceMotrice=puissanceMotrice;
		this.moteur=moteur;
		this.peri=peri;
		}
	
	public Bateau(String nom, String immatriculation,String type,Date date, 
			String etat, int capacite, int poids, 
			float puissanceMotrice,boolean moteur,Peripherique peri){
		this.nom=nom;
		this.immatriculation=immatriculation;
		this.type=type;
		this.date=date;
		this.etat=etat;
		this.capacite=capacite;
		this.poids=poids;
		this.puissanceMotrice=puissanceMotrice;
		this.moteur=moteur;
		this.peri=peri;
		}
	
	public void setId(int id){this.id=id;}
	public void setNom(String nom){this.nom=nom;}
	public void setImmatriculation(String immatriculation){this.immatriculation=immatriculation;}
	public void setType(String type){this.type=type;}
	public void setDate(Date date){this.date=date;}
	public void setCapacite(int capacite){this.capacite=capacite;}
	public void setPoids(int poids){this.poids=poids;}
	public void setPuissanceMotrice(float puissanceMotrice){this.puissanceMotrice=puissanceMotrice;}
	public void setMoteur(boolean moteur){this.moteur=moteur;}
	public void setIdPeri(Peripherique peripherique){this.peri=peripherique;}
	public int getId(){return this.id;}
	public String getNom(){return this.nom;}
	public String getImmatriculation(){return this.immatriculation;}
	public String getType(){return this.type;}
	public String getEtat(){return this.etat;}
	
	public Date getDate(){return this.date;}
	public int getCapacite(){return this.capacite;}
	public int getPoids(){return this.poids;}
	public float getPuissanceMotrice(){return this.puissanceMotrice;}
	public boolean getMoteur(){return this.moteur;}
	public Peripherique getPeri(){return this.peri;}
	
	
}
