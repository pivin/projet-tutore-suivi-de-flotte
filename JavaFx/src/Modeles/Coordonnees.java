package Modeles;

import java.sql.Date;

public class Coordonnees {
	private Bateau bateau;
	private float longitude;
	private float latitude;
	private Zone zone;
	private Date dateHoraire;
	
	public Coordonnees(Bateau bateau,float longitude,float latitude,Zone zone, Date dateHoraire){
		this.bateau=bateau;
		this.longitude=longitude;
		this.latitude=latitude;
		this.zone=zone;
		this.dateHoraire=dateHoraire;}
	
	
	public Bateau getBateau(){return this.bateau;}
	public float getLongitude(){return this.longitude;}
	public float getLatitude(){return this.latitude;}
	public Zone getZone(){return this.zone;}
	public Date getDate(){return this.dateHoraire;}
}
