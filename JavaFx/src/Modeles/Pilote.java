package Modeles;

import java.sql.Date;

public class Pilote {
	private int id;
	private String nom;
	private String prenom;
	private Date date;
	private String numeroPersonnel;
	private String numeroUrgence;
	private Permi permi;

	public Pilote(int id,String nom, String prenom,Date date,String numeroPersonnel,String numeroUrgence,
			Permi permi){
		this.id=id;
		this.nom=nom;
		this.prenom=prenom;
		this.date=date;
		this.numeroPersonnel=numeroPersonnel;
		this.numeroUrgence=numeroUrgence;
		this.permi=permi;}
	
	public Pilote(String nom, String prenom,Date date,String numeroPersonnel,String numeroUrgence,
			Permi permi){
		this.nom=nom;
		this.prenom=prenom;
		this.date=date;
		this.numeroPersonnel=numeroPersonnel;
		this.numeroUrgence=numeroUrgence;
		this.permi=permi;}

	public int getId(){return id;}
	public String getNom(){return nom;}
	public Date getDate(){return date;}
	public String getPrenom(){return prenom;}
	public String getNumeroPersonnel(){return numeroPersonnel;}
	public String getNumeroUrgence(){return numeroUrgence;}
	public Permi getPermi(){return permi;}
	public void setId(int id){this.id=id;}
	
}
