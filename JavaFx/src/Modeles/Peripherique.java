package Modeles;

public class Peripherique {
	private int id;
	private String fonction;
	private String type;
	private String numeroTel;
	
public Peripherique(int id, String fonction, String type,String numeroTel){
	this.id=id;
	this.fonction=fonction;
	this.type=type;
	this.numeroTel=numeroTel;}


public Peripherique(String fonction, String type,String numeroTel){
	this.fonction=fonction;
	this.type=type;
	this.numeroTel=numeroTel;}


public void setId(int id){this.id=id;}
public int getId(){return this.id;}
public String getFonction(){return this.fonction;}
public String getType(){return this.type;}
public String getNumeroTel(){return this.numeroTel;}


}