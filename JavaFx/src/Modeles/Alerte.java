package Modeles;

import java.sql.Date;

public class Alerte {
	private int id;
	private String type;
	private String message;
	private Peripherique expediteur;
	private Peripherique destinataire;
	private Date dateEnvoi;
	private Date dateDestinataire;
	
	public Alerte(String type,String message,Peripherique expediteur,
			Peripherique destinataire,Date dateEnvoi,Date dateDestinataire){
		this.type=type;
		this.message=message;
		this.expediteur=expediteur;
		this.destinataire=destinataire;
		this.dateEnvoi=dateEnvoi;
		this.dateDestinataire=dateDestinataire;
		}
	
	public Alerte(int id,String type,String message,Peripherique expediteur,
			Peripherique destinataire,Date dateEnvoi,Date dateDestinataire){
		this.id=id;
		this.type=type;
		this.message=message;
		this.expediteur=expediteur;
		this.destinataire=destinataire;
		this.dateEnvoi=dateEnvoi;
		this.dateDestinataire=dateDestinataire;
		}
	
	public void setId(int id){this.id=id;}
	public int getId(){return this.id;}
	public String getType(){return this.type;}
	public String getMessage(){return this.message;}
	public Peripherique getExpediteur(){return this.expediteur;}
	public Peripherique getDestinataire(){return this.destinataire;}
	public Date getDateEnvoi(){return dateEnvoi;}
	public Date getDateDestinataire(){return dateDestinataire;}
}
