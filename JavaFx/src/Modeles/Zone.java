package Modeles;

import java.sql.Date;

public class Zone {
	private int id;
	private String nom;
	private String type;
	private Date date;
	
	public Zone(int id, String nom,String type,Date date){
		this.id=id;
		this.nom=nom;
		this.type=type;
		this.date=date;
	}
	
	public Zone( String nom,String type,Date date){
		this.nom=nom;
		this.type=type;
		this.date=date;
	}
	
	public void setId(int id){this.id=id;}
	public int getId(){return this.id;}
	public String getNom(){return this.nom;}
	public String getType(){return this.type;}
	public Date getDate(){return this.date;};
}
