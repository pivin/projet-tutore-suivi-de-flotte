package Modeles;

import java.sql.Date;

public class Utilisation {
	private Date date;
	private Pilote pilote;
	private Bateau bateau;
	
	public Utilisation(Date date,Pilote pilote,Bateau bateau){
		this.date=date;
		this.pilote=pilote;
		this.bateau=bateau;
		
	}
	public Date getDate(){return this.date;}
	public Pilote getPilote(){return this.pilote;}
	public Bateau getBateau(){return this.bateau;}

}