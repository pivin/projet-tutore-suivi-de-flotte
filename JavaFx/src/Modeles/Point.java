package Modeles;

public class Point {
	private int id;
	private int abscisse; 
	private int ordonnee;
	private Zone zone;
	
	public Point(int id,int abscisse, int ordonnee,Zone zone){
		this.id=id;
		this.abscisse=abscisse;
		this.ordonnee=ordonnee;
		this.zone=zone;
	}
	
	public Point(int abscisse, int ordonnee,Zone zone){
		this.abscisse=abscisse;
		this.ordonnee=ordonnee;
		this.zone=zone;
	}
	
	public void setId(int id){this.id=id;}
	public int getId(){return this.id;}
	public int getAbscisse(){return this.abscisse;}
	public int getOrdonnee(){return this.ordonnee;}
	public Zone getZone(){return this.zone;}
}
