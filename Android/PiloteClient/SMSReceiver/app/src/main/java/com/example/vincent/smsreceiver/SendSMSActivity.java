package com.example.vincent.smsreceiver;

import android.app.Activity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vincent on 09/04/2015.
 */
public class SendSMSActivity extends Activity {
    List messages;
    Button button;
    //EditText editPhoneNum;
    //EditText editSMS;
    Spinner listSMS = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.send_sms);

        //button = (Button) findViewById(R.id.button);
        //editPhoneNum = (EditText) findViewById(R.id.editPhoneNum);
        //editSMS = (EditText) findViewById(R.id.editSMS);
        listSMS = (Spinner) findViewById(R.id.listSMS);
        messages = new ArrayList();
        messages.add("Demande amarrage");
        messages.add("Arrêt");
        messages.add("Panne");

        ArrayAdapter adapter = new ArrayAdapter(this,android.R.layout.simple_spinner_item, messages);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        listSMS.setAdapter(adapter);

        button.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {

                String phoneNo = "0604413772";
                //String phoneNo = editPhoneNum.getText().toString();
                //String sms = editSMS.getText().toString();
                String msgs = "#WAR#"+ (listSMS.getSelectedItemPosition()+1);
                try {
                    SmsManager smsManager = SmsManager.getDefault();
                    smsManager.sendTextMessage(phoneNo, null, msgs, null, null);
                    Toast.makeText(getApplicationContext(), "SMS Sent! " + msgs,
                            Toast.LENGTH_LONG).show();
                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(),
                            "SMS faild, please try again later!",
                            Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }

            }
        });
    }
}
