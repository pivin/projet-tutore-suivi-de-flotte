package com.example.vincent.smsreceiver;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vincent on 09/04/2015.
 */
public class ListSMSActivity extends Activity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_sms);
        Intent intent= getIntent();
        List<String> getList = new ArrayList<String>();
        getList = intent.getStringArrayListExtra("laliste");

        TextView tv=(TextView)findViewById(R.id.listSmsTv);
        String values = "";
        for(int i = 0 ; i < getList.size() ;  ++i) {
            values += getList.get(i) + "\n";
        }

        tv.setText(values);
    }
}
