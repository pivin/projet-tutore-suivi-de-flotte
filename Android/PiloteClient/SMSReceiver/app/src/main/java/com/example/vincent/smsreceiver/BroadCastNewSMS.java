package com.example.vincent.smsreceiver;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.app.Activity;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class BroadCastNewSMS extends Activity
{
    static ArrayList<String> list=new ArrayList<String>();
    public BroadCastNewSMS(){
        //this.list=list;

    }

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

    }
    public void startSMSActivity(View view){
        Log.i("Activity", "button pressed");
        Intent intent=new Intent(this, ListSMSActivity.class);
        intent.putStringArrayListExtra("laliste", list);
        startActivity(intent);
    }

    public void startSendSMSActivity(View view){
        Log.i("Activity", "button pressed");
        Intent intent=new Intent(this, SendSMSActivity.class);
        startActivity(intent);
    }

    //AUTO POS
    public void smsAuto(View v) {

        new AsyncTaskTest().execute();
        findViewById(R.id.buttonAutoPOS).setEnabled(false);
        Toast.makeText(getApplicationContext(),
                "Begin auto position notification...",
                Toast.LENGTH_LONG).show();
    }

    public void envoieSms(){
        GPSTracker gps = new GPSTracker(getApplicationContext());
        double latitude = 0;
        double longitude = 0;
        if (gps.canGetLocation()) {
            latitude = gps.getLatitude();
            longitude = gps.getLongitude();
        } else {
            gps.showSettingsAlert();
        }
        String phoneNo = "0604413772";
        String sms = "#POS#" + latitude + "#" + longitude;
        try {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(phoneNo, null, sms, null, null);
            Toast.makeText(getApplicationContext(), "Position sent",
                    Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(),
                    "SMS failed, please try again later!",
                    Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }
    private class AsyncTaskTest extends AsyncTask<Void, Integer, Void>
    {
        AsyncTaskTest(){
            //((TextView)findViewById(R.id.tv0)).setText("AsyncTask started...");
        }
        protected Void doInBackground(Void... params){
            try {
                for( int i = 1 ; i < 6 ; ++i ) {
                    Thread.sleep(30000);
                    publishProgress(i);
                    //publishProgress(i);
                }
            }
            catch(Exception e){
                Log.e("AsyncTaskTest", "InterruptedException" + e);
            }
            return null;
        }
        protected void onProgressUpdate(Integer... progress)
        {
            envoieSms();
            Toast.makeText(getApplicationContext(), "TOAST", Toast.LENGTH_SHORT).show();
            //((TextView)findViewById(R.id.tv0)).setText("Progress = " + progress[0]);
        }
        protected void onPostExecute(Void result){
            //(( TextView )findViewById(R.id.tv0)).setText("AsyncTask terminated");
            findViewById(R.id.buttonAutoPOS).setEnabled(true);
        }
    }
}
