package com.example.vincent.androidtcp;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.app.Activity;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class BroadCastNewSMS extends Activity
{
    static ArrayList<String> list=new ArrayList<String>();
    public static TCPClient mTcpClient;
    public BroadCastNewSMS(){
        //this.list=list;

    }

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_broad_cast_new_sms);

        // connect to the server
        new connectTask().execute("");
    }
    public void startSMSActivity(View view){
        Log.i("Activity", "button pressed");
        Intent intent=new Intent(this, ListSMSActivity.class);
        intent.putStringArrayListExtra("laliste", list);
        startActivity(intent);
    }

    public void startSendSMSActivity(View view){
        Log.i("Activity", "button pressed");
        Intent intent=new Intent(this, SendSMSActivity.class);
        startActivity(intent);
    }


    public class connectTask extends AsyncTask<String,String,TCPClient> {

        @Override
        protected TCPClient doInBackground(String... message) {

            //we create a TCPClient object and
            mTcpClient = new TCPClient(new TCPClient.OnMessageReceived() {
                @Override
                //here the messageReceived method is implemented
                public void messageReceived(String message) {
                    //this method calls the onProgressUpdate
                    publishProgress(message);
                }
            });
            mTcpClient.run();

            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);


            // split
            String[] mots=values[0].split("#");

            // recup numéro
            String phoneNumber = mots[3];

            // on vire le numéro
            String res = "#"+mots[1]+"#"+mots[2];

            try {
                SmsManager smsManager = SmsManager.getDefault();
                smsManager.sendTextMessage(phoneNumber, null, res, null, null);
                Toast.makeText(getApplicationContext(), "SMS Sent! " + res,
                        Toast.LENGTH_LONG).show();
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(),
                        "SMS failed, please try again later!",
                        Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }

            //in the arrayList we add the messaged received from server
            //arrayList.add(values[0]);
            // notify the adapter that the data set has changed. This means that new message received
            // from server was added to the list
            //mAdapter.notifyDataSetChanged();
        }
    }

}
