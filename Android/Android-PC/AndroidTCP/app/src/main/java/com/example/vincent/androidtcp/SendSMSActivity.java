package com.example.vincent.androidtcp;

import android.app.Activity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vincent on 09/04/2015.
 */
public class SendSMSActivity extends Activity {
    List messages;
    List boats;
    Button button;
    Spinner listBoats;
    //EditText editSMS;
    Spinner listSMS = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.send_sms);

        button = (Button) findViewById(R.id.button);
        listBoats = (Spinner) findViewById(R.id.listBoats);
        //editSMS = (EditText) findViewById(R.id.editSMS);
        listSMS = (Spinner) findViewById(R.id.listSMS);

        boats = new ArrayList();
        boats.add("0644793109");
        boats.add("0673651269");
        boats.add("0605472023");
        boats.add("0632893876");

        messages = new ArrayList();
        messages.add("Amarrage OK");
        messages.add("Retour au quai");
        messages.add("Sortie zone");


        ArrayAdapter adapter2 = new ArrayAdapter(this,android.R.layout.simple_spinner_item, boats);
        ArrayAdapter adapter = new ArrayAdapter(this,android.R.layout.simple_spinner_item, messages);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        listSMS.setAdapter(adapter);
        listBoats.setAdapter(adapter2);

        button.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {

                String phoneN = (String) boats.get(listBoats.getSelectedItemPosition());
                //String sms = editSMS.getText().toString();
                String msgs = "#WAR#"+(listSMS.getSelectedItemPosition()+1);
                try {
                    SmsManager smsManager = SmsManager.getDefault();
                    smsManager.sendTextMessage(phoneN, null, msgs, null, null);
                    Toast.makeText(getApplicationContext(), "SMS Sent! " + msgs,
                            Toast.LENGTH_LONG).show();
                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(),
                            "SMS failed, please try again later!",
                            Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }

            }
        });
    }
}
