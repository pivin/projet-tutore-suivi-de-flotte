package com.example.vincent.androidtcp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by rietz on 21/03/15.
 */
public class IncomingSms extends BroadcastReceiver {
    BroadCastNewSMS bc=new BroadCastNewSMS();
    // Get the object of SmsManager
    final SmsManager sms = SmsManager.getDefault();

    public void onReceive(Context context, Intent intent) {

        // Retrieves a map of extended data from the intent.
        final Bundle bundle = intent.getExtras();

        try {

            if (bundle != null) {

                final Object[] pdusObj = (Object[]) bundle.get("pdus");

                for (int i = 0; i < pdusObj.length; i++) {

                    SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                    String phoneNumber = currentMessage.getDisplayOriginatingAddress();
                    String senderNum = phoneNumber;

                    //le message envoyé tel quel
                    String message = currentMessage.getDisplayMessageBody();

                    String[] alertes={"Demande amarrage", "Arrêt", "Panne"};

                    //on vérifie le premier caractère pour vérifier la provenance du message
                    String first=message.charAt(0)+"";
                    String res="default2";
                    if(first.equals("#")){
                        String[] mots=message.split("#");

                        if(mots[1].equals("WAR")) {
                            //res = mots[2];
                            switch (mots[2]) {
                                case "1":
                                    res = "Alerte: " + alertes[0];
                                    break;
                                case "2":
                                    res = "Alerte: " + alertes[1];
                                    break;
                                case "3":
                                    res = "Alerte: " + alertes[2];
                                    break;
                            }
                        }
                        if(mots[1].equals("POS")) {
                            res = "Position: " + mots[2]+"|"+mots[3];
                        }

                        res+=" / Bateau : "+senderNum;

                        Log.i("SmsReceiver", "senderNum: " + senderNum + "; message: " + message);


                        // Show Alert
                        int duration = Toast.LENGTH_LONG;
                        Toast toast = Toast.makeText(context,
                                "senderNum: "+ senderNum + ", message: " + res, duration);
                        toast.show();
                        if (BroadCastNewSMS.mTcpClient != null) {
                            BroadCastNewSMS.mTcpClient.sendMessage(res);
                        }
                        bc.list.add(res);
                    }



                } // end for loop
            } // bundle is null

        } catch (Exception e) {
            Log.e("SmsReceiver", "Exception smsReceiver" +e);

        }
    }
}

