import java.util.Date;

public class Pilote
{
	private final int identifiant;
	private static int nombre_identifiants;
	private String nom;
	private String prenom;
	private Date date_diplome;
	private String numero_personel;
	private String numero_travail;
	
	public Pilote(String nom)
	{
		this.nom=nom;
		identifiant=nombre_identifiants++;
	}
}
