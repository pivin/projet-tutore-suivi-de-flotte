public class Position
{
	private static int longitude;
	private static int latitude;
	
	public Position(int longitude,int latitude)
	{
		this.longitude=longitude;
		this.latitude=latitude;
	}
	
	public static int[] get_coordonnees()
	{
		int[]coordonnees=new int[2];
		coordonnees[0]=longitude;
		coordonnees[1]=latitude;
		return coordonnees;
	}
}
