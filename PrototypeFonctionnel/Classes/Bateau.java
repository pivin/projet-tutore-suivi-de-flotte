import java.util.Date;

public class Bateau
{
	private final int identifiant;
	private static int nombre_identifiants;
	private String nom;
	private String immatriculation;
	private String type;
	private Date annee_achat;
	private String etat;
	
	public Bateau(String nom)
	{
		this.nom=nom;
		identifiant=nombre_identifiants++;
	}
	
	public String toString()
	{
		return ""+identifiant+","+nom;
	}
}
